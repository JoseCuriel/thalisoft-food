package com.thalisoft.main.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Jose Felix
 */
public class FiltrarDatos {

    private void guardaTabla(JTable table) {
        try {
            String sucursalesCSVFile = "src/archivos/DatosTabla.txt";
            BufferedWriter bfw = new BufferedWriter(new FileWriter(sucursalesCSVFile));

            for (int i = 0; i < table.getRowCount(); i++) //realiza un barrido por filas.
            {
                for (int j = 0; j < table.getColumnCount(); j++) //realiza un barrido por columnas.
                {
                    bfw.write((String) (table.getValueAt(i, j)));
                    if (j < table.getColumnCount() - 1) { //agrega separador "," si no es el ultimo elemento de la fila.
                        bfw.write(",");
                    }
                }
                bfw.newLine(); //inserta nueva linea.
            }

            bfw.close(); //cierra archivo!
            System.out.println("El archivo fue salvado correctamente!");
        } catch (IOException e) {
            System.out.println("ERROR: Ocurrio un problema al salvar el archivo!" + e.getMessage());
        }
    }

}
