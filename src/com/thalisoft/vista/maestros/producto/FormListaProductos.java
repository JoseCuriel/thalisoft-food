package com.thalisoft.vista.maestros.producto;

import com.thalisoft.controller.index.ControllerContenedor;
import com.thalisoft.main.util.CambiaFormatoTexto;
import com.thalisoft.main.util.Edicion;
import com.thalisoft.main.util.report.Manager_Report;
import com.thalisoft.model.maestros.producto.ProductoDao;
import com.thalisoft.vista.compra.FormProductoFinal;
import com.thalisoft.vista.maestros.producto.receta.FormListaReceta;
import com.thalisoft.vista.maestros.producto.receta.FormReceta;
import com.thalisoft.vista.preventa.cotizacion.FormCotizacion;
import com.thalisoft.vista.preventa.ordenpedido.FormOrdenPedido;
import com.thalisoft.vista.preventa.plansepare.FormPlanSepare;
import com.thalisoft.vista.ventas.FormFacturaVenta;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Jose Felix
 */
public class FormListaProductos extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    ProductoDao Pdao;
    private TableRowSorter trsFiltro;
    int opcionFiltro = 2;
    Manager_Report report = new Manager_Report();
    FormFacturaVenta formFacturaVenta;
    FormOrdenPedido formOrdenPedido;
    FormPlanSepare formPlanSepare;
    FormCotizacion formCotizacion;
    FormProductoFinal formProductoFinal;

    public FormListaProductos() {
        initComponents();
        ACCIONES_FORMULARIO();
    }

    public FormListaProductos(FormFacturaVenta formFacturaVenta, FormOrdenPedido formOrdenPedido, FormPlanSepare formPlanSepare, FormCotizacion formCotizacion,
            FormProductoFinal formProductoFinal) {
        this.formFacturaVenta = formFacturaVenta;
        this.formOrdenPedido = formOrdenPedido;
        this.formPlanSepare = formPlanSepare;
        this.formCotizacion = formCotizacion;
        this.formProductoFinal = formProductoFinal;
        initComponents();
        ACCIONES_FORMULARIO();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_LISTAPRODUCTOS = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jButton3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        radioCategoria = new javax.swing.JRadioButton();
        radiodescripcion = new javax.swing.JRadioButton();
        txtfiltro = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtcostototal = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        LB_ITEM = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtVenta = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtUtilidad = new javax.swing.JTextField();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/tag-mod.png"))); // NOI18N
        jMenuItem1.setText("Modificar");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);
        jPopupMenu1.add(jSeparator2);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/invoice.png"))); // NOI18N
        jMenuItem2.setText("IMPRIMIR ETIQUETA");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/articulo-icono-9036-32.png"))); // NOI18N
        jMenuItem3.setText("RECETA");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem3);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Listado de Productos");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 102), 2), "REFERENCIAS REGISTRADAS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 24))); // NOI18N

        TB_LISTAPRODUCTOS.setBackground(new java.awt.Color(204, 204, 255));
        TB_LISTAPRODUCTOS.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        TB_LISTAPRODUCTOS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No. Ficha", "No. Referencia", "Descripción", "Stock Actual", "Costo Unid", "Precio Venta", "Utilidad Unid", "Total Costo", "Total Venta", "Total Utilidad", "Categoria/SubCatategoria"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_LISTAPRODUCTOS.setComponentPopupMenu(jPopupMenu1);
        TB_LISTAPRODUCTOS.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        TB_LISTAPRODUCTOS.setRowHeight(26);
        TB_LISTAPRODUCTOS.setSelectionBackground(new java.awt.Color(255, 255, 51));
        TB_LISTAPRODUCTOS.setSelectionForeground(new java.awt.Color(0, 0, 0));
        TB_LISTAPRODUCTOS.getTableHeader().setReorderingAllowed(false);
        TB_LISTAPRODUCTOS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TB_LISTAPRODUCTOSMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TB_LISTAPRODUCTOS);
        if (TB_LISTAPRODUCTOS.getColumnModel().getColumnCount() > 0) {
            TB_LISTAPRODUCTOS.getColumnModel().getColumn(0).setMaxWidth(50);
            TB_LISTAPRODUCTOS.getColumnModel().getColumn(1).setMaxWidth(65);
            TB_LISTAPRODUCTOS.getColumnModel().getColumn(2).setMinWidth(300);
            TB_LISTAPRODUCTOS.getColumnModel().getColumn(3).setMaxWidth(75);
            TB_LISTAPRODUCTOS.getColumnModel().getColumn(10).setMinWidth(200);
        }

        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/restauran/food-3.png"))); // NOI18N
        jButton1.setToolTipText("Nueva Referencia");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/barcode-tag.png"))); // NOI18N
        jButton4.setToolTipText("Listado de Etiquetas");
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jButton2.setToolTipText("Imprimir Referencias");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/invoice.png"))); // NOI18N
        jButton5.setToolTipText("Imprimir Etiquetas en Impresora de Tikect");
        jButton5.setFocusable(false);
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/restaurant-232x32.png"))); // NOI18N
        jButton6.setToolTipText("Recetas");
        jButton6.setFocusable(false);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/articulo-icono-9036-32.png"))); // NOI18N
        jButton7.setToolTipText("Listado de Recetas");
        jButton7.setFocusable(false);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton7);
        jToolBar1.add(jSeparator1);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton3.setToolTipText("Salir");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Filtrar Por"));

        buttonGroup1.add(radioCategoria);
        radioCategoria.setText("Categoria");

        buttonGroup1.add(radiodescripcion);
        radiodescripcion.setSelected(true);
        radiodescripcion.setText("Descripción");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(txtfiltro)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(radiodescripcion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(radioCategoria)
                .addGap(38, 38, 38))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioCategoria)
                    .addComponent(radiodescripcion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtfiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel1.setText("COSTO DE INVENTARIO ACTUAL");

        txtcostototal.setEditable(false);
        txtcostototal.setFont(new java.awt.Font("Arial Narrow", 1, 24)); // NOI18N
        txtcostototal.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtcostototal.setText("$ 0");

        jLabel2.setText("ITEM ACTUALES:");

        LB_ITEM.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LB_ITEM.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_ITEM.setText("0");

        jLabel3.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel3.setText("VENTA DE INVENTARIO ACTUAL");

        txtVenta.setEditable(false);
        txtVenta.setFont(new java.awt.Font("Arial Narrow", 1, 24)); // NOI18N
        txtVenta.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtVenta.setText("$ 0");

        jLabel4.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel4.setText("UTILIDAD DE INVENTARIO ACTUAL");

        txtUtilidad.setEditable(false);
        txtUtilidad.setFont(new java.awt.Font("Arial Narrow", 1, 24)); // NOI18N
        txtUtilidad.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtUtilidad.setText("$ 0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LB_ITEM, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtcostototal, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(txtUtilidad, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcostototal, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUtilidad)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LB_ITEM, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        OPEN_FRM_REFERENCIA();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        FormProducto fp = new FormProducto(this);
        JInternalFrame ji = validador.getJInternalFrame(FormProducto.class.getName());

        if (ji == null || ji.isClosed()) {
            if (fp.CONSULTA_PRODUCTO(TB_LISTAPRODUCTOS.getValueAt(TB_LISTAPRODUCTOS.getSelectedRow(), 0)) != false) {
                fp.LOAD_PRODUCT_COMPONET();
            }

            ji = fp;

            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormProducto.class.getName(), ji);

            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormProducto.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        OPEN_FRM_REFERENCIA();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void TB_LISTAPRODUCTOSMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TB_LISTAPRODUCTOSMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2) {
            CambiaFormatoTexto cft = new CambiaFormatoTexto();
            Object referencia = TB_LISTAPRODUCTOS.getValueAt(TB_LISTAPRODUCTOS.getSelectedRow(), 1);
            Object descripcion = TB_LISTAPRODUCTOS.getValueAt(TB_LISTAPRODUCTOS.getSelectedRow(), 2);
            Object stock = TB_LISTAPRODUCTOS.getValueAt(TB_LISTAPRODUCTOS.getSelectedRow(), 3);
            Object valorund = TB_LISTAPRODUCTOS.getValueAt(TB_LISTAPRODUCTOS.getSelectedRow(), 5);

            if (formFacturaVenta != null) {
                formFacturaVenta.txtreferencia.setText(referencia.toString());
                formFacturaVenta.comboproducto.setSelectedItem(descripcion.toString());
                formFacturaVenta.txtstock.setText(stock.toString());
                formFacturaVenta.txtprecioventa.setText("$ " + cft.numerico(valorund.toString()));
                formFacturaVenta.txtcantidad.selectAll();
                formFacturaVenta.txtcantidad.requestFocus();
                dispose();
            }

            if (formOrdenPedido != null) {
                formOrdenPedido.txtreferencia.setText(referencia.toString());
                formOrdenPedido.comboproducto.setSelectedItem(descripcion.toString());
                formOrdenPedido.txtvalorunidad.setText("$ " + cft.numerico(valorund.toString()));
                formOrdenPedido.txtcantidad.selectAll();
                formOrdenPedido.txtcantidad.requestFocus();
                dispose();
            }

            if (formPlanSepare != null) {
                formPlanSepare.txtreferencia.setText(referencia.toString());
                formPlanSepare.comboproducto.setSelectedItem(descripcion.toString());
                formPlanSepare.txtvalorunidad.setText("$ " + cft.numerico(valorund.toString()));
                formPlanSepare.txtcantidad.selectAll();
                formPlanSepare.txtcantidad.requestFocus();
                dispose();
            }

            if (formCotizacion != null) {
                formCotizacion.txtreferencia.setText(referencia.toString());
                formCotizacion.comboproducto.setSelectedItem(descripcion.toString());
                formCotizacion.txtvalorunidad.setText("$ " + cft.numerico(valorund.toString()));
                formCotizacion.txtcantidad.selectAll();
                formCotizacion.txtcantidad.requestFocus();
                dispose();
            }

            if (formProductoFinal != null) {
                formProductoFinal.txtreferencia.setText(referencia.toString());
                formProductoFinal.comboproducto.setSelectedItem(descripcion.toString());
                formProductoFinal.txtcostound.setText("$ " + cft.numerico(valorund.toString()));
                formProductoFinal.txtcantidad.selectAll();
                formProductoFinal.txtcantidad.requestFocus();
                dispose();
            }
        }

    }//GEN-LAST:event_TB_LISTAPRODUCTOSMouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        report.LISTADO_PRODUCTOS_ETIQUETAS("%" + txtfiltro.getText() + "%");
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        report.LISTADO_PRODUCTOS();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        report.LISTADO_PRODUCTOS_ETIQUETAS_EN_TICKET("%" + txtfiltro.getText() + "%");
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        report.LISTADO_PRODUCTOS_ETIQUETAS_EN_TICKET("%" + TB_LISTAPRODUCTOS.getValueAt(TB_LISTAPRODUCTOS.getSelectedRow(), 1) + "%");
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormReceta.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormReceta();

            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormReceta.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormReceta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormListaReceta.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormListaReceta(TB_LISTAPRODUCTOS.getValueAt(TB_LISTAPRODUCTOS.getSelectedRow(), 1).toString());
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormListaReceta.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormListaReceta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormListaReceta.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormListaReceta(null);
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormListaReceta.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormListaReceta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton7ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LB_ITEM;
    private javax.swing.JTable TB_LISTAPRODUCTOS;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JRadioButton radioCategoria;
    private javax.swing.JRadioButton radiodescripcion;
    private javax.swing.JTextField txtUtilidad;
    private javax.swing.JTextField txtVenta;
    private javax.swing.JTextField txtcostototal;
    private javax.swing.JTextField txtfiltro;
    // End of variables declaration//GEN-END:variables

    private void OPEN_FRM_REFERENCIA() {
        JInternalFrame ji = validador.getJInternalFrame(FormProducto.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormProducto(this);
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormProducto.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormProducto.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void LLENAR_LISTADO_DE_REFERENCIAS() {
        edicion.llenarTabla(TB_LISTAPRODUCTOS, Pdao.LISTADO_DE_PRODUCTOS());
        TOTALES_INVENTARIO_ACTUAL();
    }

    private void ACCIONES_FORMULARIO() {
        Pdao = new ProductoDao();
        LLENAR_LISTADO_DE_REFERENCIAS();
        txtfiltro.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(final KeyEvent e) {
                String cadenafiltra = (txtfiltro.getText());
                txtfiltro.setText(cadenafiltra.toUpperCase());
                repaint();
                filtro();
            }

            private void filtro() {
                if (radiodescripcion.isSelected()) {
                    opcionFiltro = 2;
                }
                if (radioCategoria.isSelected()) {
                    opcionFiltro = 10;
                }
                if (radiodescripcion.isSelected() | radioCategoria.isSelected()) {
                    trsFiltro.setRowFilter(RowFilter.regexFilter(txtfiltro.getText(), opcionFiltro));
                    TOTALES_INVENTARIO_ACTUAL();
                } else {
                    edicion.mensajes(1, "selecciona una opcion para filtrar.");
                }
            }

        });
        trsFiltro = new TableRowSorter(TB_LISTAPRODUCTOS.getModel());
        TB_LISTAPRODUCTOS.setRowSorter(trsFiltro);
        txtfiltro.requestFocus();
    }

    private void TOTALES_INVENTARIO_ACTUAL() {
        edicion.calcula_total(TB_LISTAPRODUCTOS, LB_ITEM, txtcostototal, 7);
        edicion.calcula_total(TB_LISTAPRODUCTOS, LB_ITEM, txtVenta, 8);
        edicion.calcula_total(TB_LISTAPRODUCTOS, LB_ITEM, txtUtilidad, 9);
    }

}

class validador {

    public static java.util.HashMap<String, javax.swing.JInternalFrame> jIframes = new java.util.HashMap<>();

    public static void addJIframe(String key, javax.swing.JInternalFrame jiframe) {
        jIframes.put(key, jiframe);
    }

    public static javax.swing.JInternalFrame getJInternalFrame(String key) {
        return jIframes.get(key);
    }
}
