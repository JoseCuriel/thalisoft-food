package com.thalisoft.vista.maestros.producto;

import com.tekde.linet.components.JFileChooserCus;
import com.tekde.linet.components.JFileChooserCusListener;
import com.thalisoft.controller.index.ControllerContenedor;
import com.thalisoft.main.util.CambiaFormatoTexto;
import com.thalisoft.main.util.Edicion;
import com.thalisoft.main.util.ImageResizer;
import com.thalisoft.main.util.Test_CodigoAleatorio;
import com.thalisoft.main.util.Variables_Gloabales;
import com.thalisoft.main.util.report.Manager_Report;
import com.thalisoft.model.maestros.empleado.EmpleadoDao;
import com.thalisoft.model.maestros.producto.Categoria;
import com.thalisoft.model.maestros.producto.CategoriaDao;
import com.thalisoft.model.maestros.producto.Producto;
import com.thalisoft.model.maestros.producto.ProductoDao;
import com.thalisoft.model.maestros.producto.SubCategoria;
import com.thalisoft.model.maestros.producto.UnidadMedida;
import com.thalisoft.model.maestros.producto.UnidadMedidaDao;
import com.thalisoft.vista.index.Contenedor;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;

public class FormProducto extends javax.swing.JInternalFrame {

    EmpleadoDao Edao;
    Edicion edicion = new Edicion();
    Producto producto;
    ProductoDao Pdao;
    Test_CodigoAleatorio codigoAleatorio = new Test_CodigoAleatorio();
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();
    FormListaProductos formListaProductos;
    CategoriaDao categoriaDao = new CategoriaDao();
    UnidadMedidaDao medidaDao = new UnidadMedidaDao();
    private String NOMBRE_IMAGEN_PRODUCTO = "imgnodisponible.png";
    private final String RUTA_IMAGEN_PRODUCTO;

    public FormProducto() {
        this.RUTA_IMAGEN_PRODUCTO = "C:\\ThaliSoftFoodData\\Image\\Productos\\";
        Pdao = new ProductoDao();
        initComponents();
        NUEVO_PRODUCTO();
        accionesFormulario();
    }

    FormProducto(FormListaProductos aThis) {
        this.RUTA_IMAGEN_PRODUCTO = "C:\\ThaliSoftFoodData\\Image\\Productos\\";
        formListaProductos = aThis;
        Pdao = new ProductoDao();
        initComponents();
        NUEVO_PRODUCTO();
        accionesFormulario();
        java.awt.Dimension Tamaño_Panel = Contenedor.Panel_Contenedor.getSize();
        java.awt.Dimension Tamaño_InternalFrame = this.getSize();
        this.setLocation((Tamaño_Panel.width - Tamaño_InternalFrame.width) / 2,
                (Tamaño_Panel.height - Tamaño_InternalFrame.height) / 2);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtnumficha = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtreferencia = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtdescripcion = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtstock = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtcosto = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtprecioventa = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtutilidad = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        comboCategoria = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        radioProdVenta = new javax.swing.JRadioButton();
        radioProdInsumo = new javax.swing.JRadioButton();
        jLabel10 = new javax.swing.JLabel();
        comboUndMedida = new javax.swing.JComboBox();
        jButton7 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        comboSubCategoria = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        txtPUM = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtembalaje = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtstockMin = new javax.swing.JTextField();
        LB_IMAGE = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setTitle("Producto");

        jPanel1.setBackground(new java.awt.Color(204, 102, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PRODUCTO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 18), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("No. de Ficha");

        txtnumficha.setEditable(false);
        txtnumficha.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Referencia*");

        txtreferencia.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Descripción*");

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Stock Actual");

        txtstock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtstock.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtstock.setText("0");

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Costo*");

        txtcosto.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtcosto.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtcosto.setText("$ 0");

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Precio de Venta*");

        txtprecioventa.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtprecioventa.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtprecioventa.setText("$ 0");

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Utilidad");

        txtutilidad.setEditable(false);
        txtutilidad.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtutilidad.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtutilidad.setText("$ 0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtcosto, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtutilidad, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtprecioventa, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcosto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(4, 4, 4))
                    .addComponent(txtprecioventa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtutilidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35))
        );

        jPanel3.setBackground(new java.awt.Color(255, 204, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detalle de la Referencia", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N

        jLabel9.setText("CATEGORIA");

        comboCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tipo de Producto", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N

        buttonGroup1.add(radioProdVenta);
        radioProdVenta.setText("PRODUCTO PARA VENTA");

        buttonGroup1.add(radioProdInsumo);
        radioProdInsumo.setText("PRODUCTO PARA INSUMO");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(radioProdVenta)
                .addGap(18, 18, 18)
                .addComponent(radioProdInsumo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioProdVenta)
                    .addComponent(radioProdInsumo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel10.setText("UNIDAD DE MEDIDA");

        jButton7.setText("...");
        jButton7.setToolTipText("AGREGAR CATEGORIA");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jLabel11.setText("SUBCATEGORIA");

        comboSubCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "" }));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("P.U.M");

        txtPUM.setEditable(false);
        txtPUM.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtPUM.setText("0");
        txtPUM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPUMActionPerformed(evt);
            }
        });

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("EMBALAJE");

        txtembalaje.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtembalaje.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtembalaje.setText("0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtembalaje)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(comboCategoria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(comboSubCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jButton7))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(comboUndMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPUM, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtembalaje, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboUndMedida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPUM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboSubCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Stock Minimo");

        txtstockMin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtstockMin.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtstockMin.setText("0");

        LB_IMAGE.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_IMAGE.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "IMAGEN", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton8.setText("CARGAR IMAGEN");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(204, 102, 0));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-page.png"))); // NOI18N
        jButton1.setToolTipText("Nuevo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/book.png"))); // NOI18N
        jButton2.setToolTipText("Consultar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/save.png"))); // NOI18N
        jButton3.setToolTipText("Guardar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/shift-change.png"))); // NOI18N
        jButton4.setToolTipText("Modificar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/invoice.png"))); // NOI18N
        jButton6.setToolTipText("IMPRIMIR ETIQUETA");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton5.setToolTipText("Salir");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(LB_IMAGE, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtnumficha, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtreferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtdescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 421, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(txtstock, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel16)
                                .addGap(18, 18, 18)
                                .addComponent(txtstockMin, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtnumficha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtreferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtdescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtstock)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtstockMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(LB_IMAGE, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(11, 11, 11)
                        .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        NUEVO_PRODUCTO();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Object idproducto = edicion.msjQuest(2, "ingresa la referencia del producto.");
        System.out.println(idproducto);
        if (CONSULTA_PRODUCTO(idproducto) == false) {
            edicion.mensajes(1, "la referencia no esta registrada.");
        } else {
            LOAD_PRODUCT_COMPONET();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int opcion = (int) edicion.msjQuest(1, "estas seguro que desea registrar la referencia?");
        if (opcion == 0) {
            if (validarProducto() != false) {
                if (CONSULTA_PRODUCTO(txtreferencia.getText()) != true) {
                    if (Pdao.EJECUTAR_CRUD(DATOS_PRODUCTO(0)) != false) {
                        formListaProductos.LLENAR_LISTADO_DE_REFERENCIAS();
                        edicion.mensajes(2, "referencia registrado correctamente.");
                    } else {
                        edicion.mensajes(1, "la referencia # " + txtreferencia.getText() + " se encuentra registrada");
                    }
                }
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        int opcion = (int) edicion.msjQuest(1, "estas seguro que desea modificar la referencia?");
        if (opcion == 0) {
            if (validarProducto() != false) {
                if (CONSULTA_PRODUCTO(edicion.toNumeroEntero(txtnumficha.getText())) != false) {
                    if (Pdao.EJECUTAR_CRUD(DATOS_PRODUCTO(1)) != false) {
                        formListaProductos.LLENAR_LISTADO_DE_REFERENCIAS();
                        edicion.mensajes(2, "producto modificado correctamente.");
                    }
                }
            }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        Manager_Report mr = new Manager_Report();
        mr.LISTADO_PRODUCTOS_ETIQUETAS_EN_TICKET("%" + txtreferencia.getText() + "%");
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormCategoria.class.getName());

        if (ji == null || ji.isClosed()) {
            ji = new FormCategoria();
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormCategoria.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormCategoria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        CARGAR_IMAGEN_PRODUCTO();


    }//GEN-LAST:event_jButton8ActionPerformed

    private void txtPUMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPUMActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPUMActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LB_IMAGE;
    private javax.swing.ButtonGroup buttonGroup1;
    public static javax.swing.JComboBox comboCategoria;
    private javax.swing.JComboBox comboSubCategoria;
    private javax.swing.JComboBox comboUndMedida;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private static javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton radioProdInsumo;
    private javax.swing.JRadioButton radioProdVenta;
    private javax.swing.JTextField txtPUM;
    private javax.swing.JTextField txtcosto;
    private javax.swing.JTextField txtdescripcion;
    private javax.swing.JTextField txtembalaje;
    private javax.swing.JTextField txtnumficha;
    private javax.swing.JTextField txtprecioventa;
    private javax.swing.JTextField txtreferencia;
    private javax.swing.JTextField txtstock;
    private javax.swing.JTextField txtstockMin;
    private javax.swing.JTextField txtutilidad;
    // End of variables declaration//GEN-END:variables
    private boolean validarProducto() {
        if (txtreferencia.getText().isEmpty() | txtreferencia.getText() == null) {
            edicion.mensajes(1, "por favor ingresa la referencia del producto.");
            txtreferencia.grabFocus();
            return false;
        }
        if (txtdescripcion.getText().isEmpty() | txtdescripcion.getText() == null) {
            edicion.mensajes(1, "por favor ingresa la descripcion del producto.");
            txtdescripcion.grabFocus();
            return false;
        }
        if (edicion.toNumeroEntero(txtcosto.getText()) < 1 && radioProdInsumo.isSelected()) {
            edicion.mensajes(1, "el costo del producto debe ser mayor a cero (0).");
            txtcosto.selectAll();
            txtcosto.requestFocus();
            return false;
        }

        System.out.println(radioProdVenta.isSelected() + " " + comboCategoria.getSelectedItem());
        if (radioProdVenta.isSelected() && !comboCategoria.getSelectedItem().equals("PLATOS")) {

            if (edicion.toNumeroEntero(txtcosto.getText()) < 1) {
                edicion.mensajes(1, "el costo del producto debe ser mayor a cero (0).");
                txtcosto.selectAll();
                txtcosto.requestFocus();
                return false;
            }

            if (edicion.toNumeroEntero(txtprecioventa.getText()) < 1) {
                edicion.mensajes(1, "el precio de venta debe ser mayor a cero (0)");
                txtprecioventa.selectAll();
                txtprecioventa.requestFocus();
                return false;
            }
        }

        if (!radioProdInsumo.isSelected() && !radioProdVenta.isSelected()) {
            edicion.mensajes(1, "selecciona el tipo de venta del producto.");
            return false;
        }

        return true;
    }

    public void LOAD_PRODUCT_COMPONET() {
        txtnumficha.setText("" + producto.getId_producto());
        txtreferencia.setText(producto.getReferencia());
        txtdescripcion.setText(producto.getDescripcion());
        txtstock.setText(formatoTexto.numerico(producto.getStrock()));
        txtcosto.setText("$ " + CambiaFormatoTexto.numerico(producto.getCosto_und()));
        txtprecioventa.setText("$ " + formatoTexto.numerico(producto.getPrecio_venta()));
        txtutilidad.setText("$ " + formatoTexto.numerico(producto.getUtilidad()));
        txtstockMin.setText("" + producto.getStockMinimo());
        if (producto.getSubCategoria() != null) {
            comboCategoria.setSelectedItem(producto.getSubCategoria().getCategoria().getDescripcion());
            comboSubCategoria.setSelectedItem(producto.getSubCategoria().getDescripcion());
            if (producto.getTipoVenta().equals("VENTA")) {
                radioProdVenta.setSelected(true);
            } else {
                radioProdInsumo.setSelected(true);
            }
            comboUndMedida.setSelectedItem(producto.getUndMedida());
        }

        MOSTRAR_IMAGEN_PRODUCTO(producto.getImagen());
        txtPUM.setText(String.valueOf(producto.getPum()));
        txtembalaje.setText(String.valueOf(producto.getEmbalaje()));

    }

    public boolean CONSULTA_PRODUCTO(Object key) {
        producto = Pdao.READ_PRODUCTO(key);
        return producto != null;
    }

    private void NUEVO_PRODUCTO() {
        producto = new Producto();
        LOAD_PRODUCT_COMPONET();
        txtnumficha.setText(Pdao.NUMERO_FICHA_PRODUCTO());
        String codreferencia = codigoAleatorio.getCodigoAleatorioNumerico();
        if (CONSULTA_PRODUCTO(codreferencia) != true) {
            txtreferencia.setText(codreferencia);
        }

    }

    private void accionesFormulario() {

        comboSubCategoria.addItemListener(new java.awt.event.ItemListener() {

            @Override
            public void itemStateChanged(java.awt.event.ItemEvent e) {

                if (e.getStateChange() == ItemEvent.SELECTED) {

                    if (radioProdInsumo.isSelected()) {

                        CARGAR_UNIDAD_MEDIDA();
                    } else {
                        comboUndMedida.removeAllItems();
                    }

                }
            }

        });

        comboUndMedida.addItemListener(new java.awt.event.ItemListener() {

            @Override
            public void itemStateChanged(java.awt.event.ItemEvent e) {

                if (e.getStateChange() == ItemEvent.SELECTED) {

                    CALCULAR_COSTO_UND_MIN();

                }
            }

        });

        radioProdInsumo.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    if (radioProdInsumo.isSelected()) {
                        CARGAR_UNIDAD_MEDIDA();
                    } else {
                        comboUndMedida.removeAllItems();
                    }
                }
            }
        });

        for (Categoria LISTADO_DE_CATEGORIAS : categoriaDao.LISTADO_DE_CATEGORIAS()) {
            comboCategoria.addItem(LISTADO_DE_CATEGORIAS.getDescripcion());
        }

        comboCategoria.addItemListener(new java.awt.event.ItemListener() {

            @Override
            public void itemStateChanged(java.awt.event.ItemEvent e) {

                if (e.getStateChange() == ItemEvent.SELECTED) {

                    CARGAR_SUBCATEGORIA("'" + comboCategoria.getSelectedItem() + "'");
                }
            }
        });

        txtprecioventa.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtprecioventa.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtprecioventa.getText())));
                if (CALCULAR_UTILIDAD() > 0) {
                    txtutilidad.setText("$ " + formatoTexto.numerico(CALCULAR_UTILIDAD()));
                } else {
                    txtutilidad.setText("$ 0");
                }
            }
        });
        txtcosto.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtcosto.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtcosto.getText())));
                if (radioProdInsumo.isSelected()) {
                    CALCULAR_COSTO_UND_MIN();
                }
            }
        });

        txtdescripcion.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtdescripcion.setText(txtdescripcion.getText().toUpperCase());
            }
        });
    }

    private Object[] DATOS_PRODUCTO(int opcion) {
        Object[] key = new Object[16];
        producto = Cargar_Producto();
        if (opcion == 0) {
            key[0] = 0;
        }
        if (opcion == 1) {
            key[0] = 1;
        }
        if (opcion == 2) {
            key[0] = 2;
        }
        key[1] = producto.getId_producto();
        key[2] = "'" + producto.getReferencia() + "'";
        key[3] = "'" + producto.getDescripcion().toUpperCase() + "'";
        key[4] = producto.getStrock();
        key[5] = producto.getCosto_und();
        key[6] = producto.getPrecio_venta();
        key[7] = producto.getUtilidad();
        key[8] = "'" + producto.getStockMinimo() + "'";
        key[9] = "'" + producto.getTipoVenta() + "'";
        key[10] = "'" + producto.getUndMedida() + "'";
        key[11] = "'" + producto.getEmpleado().getIdentificacion() + "'";
        key[12] = producto.getSubCategoria().getIdSubCategoria();
        key[13] = "'" + producto.getImagen() + "'";
        key[14] = CALCULAR_PUM();
        key[15] = Float.parseFloat(txtembalaje.getText());

        return key;
    }

    private Producto Cargar_Producto() {
        producto = new Producto();
        producto.setId_producto(edicion.toNumeroEntero(txtnumficha.getText()));
        producto.setReferencia(txtreferencia.getText());
        producto.setDescripcion(txtdescripcion.getText());
        producto.setStrock(edicion.toNumeroEntero(txtstock.getText()));
        producto.setCosto_und(edicion.toNumeroEntero(txtcosto.getText()));
        producto.setPrecio_venta(edicion.toNumeroEntero(txtprecioventa.getText()));
        producto.setUtilidad(edicion.toNumeroEntero(txtutilidad.getText()));
        producto.setEmpleado(Variables_Gloabales.EMPLEADO);
        producto.setStockMinimo(edicion.toNumeroEntero(txtstockMin.getText()));
        producto.setSubCategoria(categoriaDao.SELECT_SUBCATEGORIA("3,'" + comboSubCategoria.getSelectedItem() + "'"));
        if (comboUndMedida.getSelectedItem() != null) {
            producto.setUndMedida(comboUndMedida.getSelectedItem().toString());
        }
        String tipoVenta;
        if (radioProdVenta.isSelected()) {
            tipoVenta = "VENTA";
        } else {
            tipoVenta = " INSUMO";
        }
        producto.setTipoVenta(tipoVenta);
        producto.setImagen(NOMBRE_IMAGEN_PRODUCTO);
        System.out.println("PUM: " + CALCULAR_PUM());
        producto.setPum(CALCULAR_PUM());
        producto.setEmbalaje(Float.parseFloat(txtembalaje.getText()));

        return producto;
    }

    private int CALCULAR_UTILIDAD() {
        return edicion.toNumeroEntero(txtprecioventa.getText()) - edicion.toNumeroEntero(txtcosto.getText());
    }

    private void CARGAR_SUBCATEGORIA(Object categoria) {
        comboSubCategoria.removeAllItems();
        comboSubCategoria.addItem(null);
        for (SubCategoria LISTADO_DE_SUBCATEGORIAS : categoriaDao.LISTADO_DE_SUBCATEGORIAS("2," + categoria)) {
            comboSubCategoria.addItem(LISTADO_DE_SUBCATEGORIAS.getDescripcion());
        }
    }

    private void CARGAR_IMAGEN_PRODUCTO() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(this);
        File file = fileChooser.getSelectedFile();
        String rutaImage = null;
        ImageResizer ir = new ImageResizer();

        if (file != null) {
            rutaImage = file.getPath();
            NOMBRE_IMAGEN_PRODUCTO = file.getName();

        } else {
            NOMBRE_IMAGEN_PRODUCTO = "imgnodisponible.png";
        }

        ir.copyImage(rutaImage, RUTA_IMAGEN_PRODUCTO + NOMBRE_IMAGEN_PRODUCTO);
        ImageIcon image = new ImageIcon(ir.resize(ir.loadImage(rutaImage), 200, 100));
        LB_IMAGE.setIcon(image);

    }

    private void MOSTRAR_IMAGEN_PRODUCTO(String nombreImagen) {
        String image = RUTA_IMAGEN_PRODUCTO + nombreImagen;
        NOMBRE_IMAGEN_PRODUCTO = nombreImagen;
        ImageIcon img = new ImageIcon(image);
        LB_IMAGE.setIcon(img);
    }

    private void CARGAR_UNIDAD_MEDIDA() {
        comboUndMedida.addItem(null);
        for (UnidadMedida LISTADO_UNIDAD_MEDIDA : medidaDao.LISTADO_UNIDAD_MEDIDA()) {
            comboUndMedida.addItem(LISTADO_UNIDAD_MEDIDA.getMagnitud());
        }

    }

    private void CALCULAR_COSTO_UND_MIN() {
        UnidadMedida unidadMedida = medidaDao.SELECT_UNIDAD_MEDIDA("1,'" + comboUndMedida.getSelectedItem() + "'");
        float costoEmbalaje = edicion.toNumeroFloat(txtcosto.getText());
        if (costoEmbalaje > 0) {
            DecimalFormat df = new DecimalFormat("#.00");
            txtPUM.setText("$ " + df.format(CALCULAR_PUM()) + " C/" + unidadMedida.getSimbolo());
        }
    }

    private float CALCULAR_PUM() {
        float pum = 0;
        if (edicion.toNumeroFloat(txtcosto.getText()) > 0 | Float.parseFloat(txtembalaje.getText()) > 0) {
            pum = edicion.toNumeroFloat(txtcosto.getText()) / Float.parseFloat(txtembalaje.getText());
        }

        return pum;
    }
}
