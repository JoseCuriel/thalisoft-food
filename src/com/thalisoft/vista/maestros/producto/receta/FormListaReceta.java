package com.thalisoft.vista.maestros.producto.receta;

import com.thalisoft.main.util.Edicion;
import com.thalisoft.model.maestros.producto.receta.RecetaDao;
import com.thalisoft.vista.index.Contenedor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author ThaliSoft
 */
public class FormListaReceta extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    RecetaDao recetaDao;
    public String ID_RECETA;
    private final TableRowSorter trsFiltro;

    public FormListaReceta(String toString) {
        this.ID_RECETA = toString;
        initComponents();
        java.awt.Dimension Tamaño_Panel = Contenedor.Panel_Contenedor.getSize();
        java.awt.Dimension Tamaño_InternalFrame = this.getSize();
        this.setLocation((Tamaño_Panel.width - Tamaño_InternalFrame.width) / 2,
                (Tamaño_Panel.height - Tamaño_InternalFrame.height) / 2);

        CARGAR_RECETA();
        TB_RECETAS.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                int row = TB_RECETAS.getSelectedRow();
                ID_RECETA = TB_RECETAS.getValueAt(row, 0).toString();
                CARGAR_RECETA();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        });

        txtfiltro.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(final KeyEvent e) {
                String cadenafiltra = (txtfiltro.getText());
                txtfiltro.setText(cadenafiltra.toUpperCase());
                repaint();
                filtro();

            }

            private void filtro() {
                int columnaABuscar = 0;
                if (radioTitulo.isSelected()) {
                    columnaABuscar = 1;
                }
                if (radioProducto.isSelected()) {
                    columnaABuscar = 2;
                }

                trsFiltro.setRowFilter(RowFilter.regexFilter(txtfiltro.getText(), columnaABuscar));
            }
        });
        trsFiltro = new TableRowSorter(TB_RECETAS.getModel());
        TB_RECETAS.setRowSorter(trsFiltro);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_RECETAS = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TB_INGREDIENTES = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        radioTitulo = new javax.swing.JRadioButton();
        radioProducto = new javax.swing.JRadioButton();
        txtfiltro = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setTitle("Listado de Recetas");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "RECETAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Verdana", 0, 18))); // NOI18N

        TB_RECETAS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID Receta", "Titulo ", "Producto Final", "Costo Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_RECETAS.setRowHeight(22);
        TB_RECETAS.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TB_RECETAS);
        if (TB_RECETAS.getColumnModel().getColumnCount() > 0) {
            TB_RECETAS.getColumnModel().getColumn(0).setMaxWidth(60);
            TB_RECETAS.getColumnModel().getColumn(1).setMinWidth(200);
            TB_RECETAS.getColumnModel().getColumn(2).setMinWidth(150);
        }

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LISTADO DE INGREDIENTES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        TB_INGREDIENTES.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Item Ingr", "Referencia", "Descripcion", "Cantidad", "Costo Porcion", "Costo Total", "Und. Medida"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_INGREDIENTES.setRowHeight(22);
        TB_INGREDIENTES.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(TB_INGREDIENTES);
        if (TB_INGREDIENTES.getColumnModel().getColumnCount() > 0) {
            TB_INGREDIENTES.getColumnModel().getColumn(2).setMinWidth(200);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 644, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2))
        );

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/embudo-filtrante-icono-4497-32.png"))); // NOI18N

        buttonGroup1.add(radioTitulo);
        radioTitulo.setText("Titulo de la Receta");

        buttonGroup1.add(radioProducto);
        radioProducto.setText("Producto Final");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 547, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(radioTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(radioProducto)
                        .addGap(18, 18, 18)
                        .addComponent(txtfiltro)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radioTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(radioProducto)
                            .addComponent(txtfiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TB_INGREDIENTES;
    private javax.swing.JTable TB_RECETAS;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JRadioButton radioProducto;
    private javax.swing.JRadioButton radioTitulo;
    private javax.swing.JTextField txtfiltro;
    // End of variables declaration//GEN-END:variables

    private void CARGAR_RECETA() {
        if (ID_RECETA != null) {
            recetaDao = new RecetaDao();
            if (recetaDao.SELECT_RECETA("0," + ID_RECETA) != null) {
                edicion.llenarTabla(TB_RECETAS, recetaDao.LISTADO_RECETAS("5," + ID_RECETA));
                edicion.llenarTabla(TB_INGREDIENTES, recetaDao.LISTADO_INGREDIENTES("1," + ID_RECETA));
            }
        } else {
            CARGAR_PRODUCTOS_RECETADOS();
        }
    }

    void CARGAR_PRODUCTOS_RECETADOS() {
        recetaDao = new RecetaDao();
        edicion.llenarTabla(TB_RECETAS, recetaDao.LISTADO_RECETAS("5," + ID_RECETA));
    }

}
