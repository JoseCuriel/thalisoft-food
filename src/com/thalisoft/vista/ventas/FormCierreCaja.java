package com.thalisoft.vista.ventas;

import com.thalisoft.main.util.CambiaFormatoTexto;
import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.Edicion;
import com.thalisoft.main.util.Variables_Gloabales;
import com.thalisoft.model.venta.caja.Caja;
import com.thalisoft.model.venta.caja.CajaDao;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jose Felix
 */
public class FormCierreCaja extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();
    Caja caja;
    CajaDao cajaDao;

    public FormCierreCaja() {
        cajaDao = new CajaDao();
        initComponents();
        JD_apertura.setDate(DateUtil.newDateTime());
        llenarcombos();

        txtsaldoinicial.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                int saldoInicial = edicion.toNumeroEntero(txtsaldoinicial.getText());
                if (saldoInicial > 0) {
                    txtsaldoinicial.setText("$ " + formatoTexto.numerico(saldoInicial));
                    TOTAL_NETO_CAJA();
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        POPUPBILLETES = new javax.swing.JPopupMenu();
        JM_ELIMINABILLETES = new javax.swing.JMenuItem();
        POPUPMONEDAS = new javax.swing.JPopupMenu();
        JM_ELIMINAMONEDAS = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnumcaja = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JD_apertura = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        JD_cierre = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        txtsaldoinicial = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        btnabrircaja = new javax.swing.JButton();
        btncerrarcaja = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jButton4 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        LB_ESTADO_CAJA = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        combobilletes = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        txtcntbillete = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtcntmoneda = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        combomonedas = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_BILLETES = new javax.swing.JTable();
        LB_BILLETE = new javax.swing.JLabel();
        txttotalbilletes = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txttotalmonedas = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txttotalcaja = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        TB_MONEDAS = new javax.swing.JTable();
        LB_MONEDA = new javax.swing.JLabel();
        LBCNTBILLETE = new javax.swing.JLabel();
        LBCNTMONEDA = new javax.swing.JLabel();

        JM_ELIMINABILLETES.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/Qx9 Vista Bin2 Full.png"))); // NOI18N
        JM_ELIMINABILLETES.setText("ELIMINAR");
        JM_ELIMINABILLETES.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JM_ELIMINABILLETESActionPerformed(evt);
            }
        });
        POPUPBILLETES.add(JM_ELIMINABILLETES);

        JM_ELIMINAMONEDAS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/Qx9 Vista Bin2 Full.png"))); // NOI18N
        JM_ELIMINAMONEDAS.setText("ELIMINAR");
        JM_ELIMINAMONEDAS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JM_ELIMINAMONEDASActionPerformed(evt);
            }
        });
        POPUPMONEDAS.add(JM_ELIMINAMONEDAS);

        setClosable(true);
        setIconifiable(true);
        setTitle("Caja");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "APERTURA & CIERRE DE CAJA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Arial Narrow", 1, 18))); // NOI18N

        jLabel1.setText("No. Caja");

        txtnumcaja.setEditable(false);
        txtnumcaja.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        txtnumcaja.setHorizontalAlignment(javax.swing.JTextField.TRAILING);

        jLabel2.setText("FECHA APERTURA");

        JD_apertura.setDateFormatString("EEE, dd MMM yyyy HH:mm:s");
        JD_apertura.setEnabled(false);

        jLabel3.setText("FECHA DE CIERRE");

        JD_cierre.setDateFormatString("EEE, dd MMM yyyy HH:mm:s");
        JD_cierre.setEnabled(false);

        jLabel4.setText("SALDO INICIAL:");

        txtsaldoinicial.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        txtsaldoinicial.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtsaldoinicial.setText("$ 0");

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/cash-register.png"))); // NOI18N

        jToolBar1.setRollover(true);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/add-page.png"))); // NOI18N
        jButton1.setToolTipText("Nueva Apertura");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        btnabrircaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/unlocked.png"))); // NOI18N
        btnabrircaja.setToolTipText("Abrir Caja");
        btnabrircaja.setFocusable(false);
        btnabrircaja.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnabrircaja.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnabrircaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnabrircajaActionPerformed(evt);
            }
        });
        jToolBar1.add(btnabrircaja);

        btncerrarcaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/lock.png"))); // NOI18N
        btncerrarcaja.setToolTipText("Cerrar Caja");
        btncerrarcaja.setFocusable(false);
        btncerrarcaja.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btncerrarcaja.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btncerrarcaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrarcajaActionPerformed(evt);
            }
        });
        jToolBar1.add(btncerrarcaja);
        jToolBar1.add(jSeparator1);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jButton4.setToolTipText("Salir");
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jLabel5.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        jLabel5.setText("ESTADO DE LA CAJA: ");

        LB_ESTADO_CAJA.setFont(new java.awt.Font("Arial Narrow", 3, 18)); // NOI18N
        LB_ESTADO_CAJA.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(txtsaldoinicial, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(JD_apertura, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtnumcaja, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JD_cierre, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LB_ESTADO_CAJA, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtnumcaja))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(JD_apertura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(JD_cierre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtsaldoinicial)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(LB_ESTADO_CAJA, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1))))
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(50, 194, 113));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DETALLE DE CIERRE", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Arial Narrow", 1, 18), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("BILLETES");

        combobilletes.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        combobilletes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        combobilletes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combobilletesItemStateChanged(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("CANTIDAD");

        txtcntbillete.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        txtcntbillete.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtcntbillete.setText("0");
        txtcntbillete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcntbilleteActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("CANTIDAD");

        txtcntmoneda.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        txtcntmoneda.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtcntmoneda.setText("0");
        txtcntmoneda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcntmonedaActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("MONEDAS");

        combomonedas.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        combomonedas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        combomonedas.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combomonedasItemStateChanged(evt);
            }
        });

        TB_BILLETES.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID. DETALLE", "BILLETES", "CANTIDAD", "TOTAL"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        TB_BILLETES.setComponentPopupMenu(POPUPBILLETES);
        TB_BILLETES.setRowHeight(22);
        jScrollPane1.setViewportView(TB_BILLETES);

        LB_BILLETE.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        txttotalbilletes.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        txttotalbilletes.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txttotalbilletes.setText("$ 0");

        jLabel10.setText("TOTAL $ EN BILLESTES");

        jLabel11.setText("TOTAL $ MODEDAS");

        txttotalmonedas.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        txttotalmonedas.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txttotalmonedas.setText("$ 0");

        jLabel12.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("TOTAL EN CAJA");

        txttotalcaja.setEditable(false);
        txttotalcaja.setBackground(new java.awt.Color(255, 255, 0));
        txttotalcaja.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        txttotalcaja.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txttotalcaja.setText("$ 0");

        TB_MONEDAS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID. DETALLE", "MONEDA", "CANTIDAD", "TOTAL"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        TB_MONEDAS.setComponentPopupMenu(POPUPMONEDAS);
        TB_MONEDAS.setRowHeight(22);
        jScrollPane2.setViewportView(TB_MONEDAS);

        LB_MONEDA.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LB_MONEDA.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        LBCNTBILLETE.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LBCNTBILLETE.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LBCNTBILLETE.setText("0");

        LBCNTMONEDA.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LBCNTMONEDA.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LBCNTMONEDA.setText("0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(63, 63, 63)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(combobilletes, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(30, 30, 30)
                                        .addComponent(txtcntbillete, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(LBCNTBILLETE, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel10)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txttotalbilletes, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(96, 96, 96)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(combomonedas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE))
                                .addGap(26, 26, 26)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtcntmoneda, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(LBCNTMONEDA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txttotalmonedas, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(LB_BILLETE, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LB_MONEDA, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(81, 81, 81))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(184, 184, 184)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txttotalcaja, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LB_BILLETE, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                    .addComponent(LB_MONEDA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combobilletes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcntbillete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(combomonedas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtcntmoneda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LBCNTBILLETE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txttotalbilletes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txttotalmonedas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LBCNTMONEDA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttotalcaja, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(54, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 714, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void combobilletesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combobilletesItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            CARGAR_IMAGEN_DIVISAS(0);
        } else {
            LB_BILLETE.setIcon(null);
        }
    }//GEN-LAST:event_combobilletesItemStateChanged

    private void combomonedasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combomonedasItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            CARGAR_IMAGEN_DIVISAS(1);
        } else {
            LB_MONEDA.setIcon(null);
        }
    }//GEN-LAST:event_combomonedasItemStateChanged

    private void txtcntbilleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcntbilleteActionPerformed
        // TODO add your handling code here:
        if (validacionBilletes() != false) {
            LLENAR_TB_BILLETES();
            TOTAL_NETO_CAJA();
        }
    }//GEN-LAST:event_txtcntbilleteActionPerformed

    private void txtcntmonedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcntmonedaActionPerformed
        // TODO add your handling code here:
        if (validacionMonedas() != false) {
            LLENAR_TB_MONEDAS();
            TOTAL_NETO_CAJA();
        }
    }//GEN-LAST:event_txtcntmonedaActionPerformed

    private void JM_ELIMINABILLETESActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JM_ELIMINABILLETESActionPerformed
        // TODO add your handling code here:
        try {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que desea eliminar el registro?");
            if (SI_NO == 0) {
                edicion.menu_emergente(TB_BILLETES);
                TOTAL_NETO_CAJA();
            }
        } catch (Exception e) {
            edicion.mensajes(3, "seleccina el billete que desea eliminar.");
        }
    }//GEN-LAST:event_JM_ELIMINABILLETESActionPerformed

    private void JM_ELIMINAMONEDASActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JM_ELIMINAMONEDASActionPerformed
        // TODO add your handling code here:
        try {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que desea eliminar el registro?");
            if (SI_NO == 0) {
                edicion.menu_emergente(TB_MONEDAS);
                TOTAL_NETO_CAJA();
            }
        } catch (Exception e) {
            edicion.mensajes(3, "seleccina la moneda que desea eliminar.");
        }
    }//GEN-LAST:event_JM_ELIMINAMONEDASActionPerformed

    private void btnabrircajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnabrircajaActionPerformed
        // TODO add your handling code here:
        int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas abrir la caja?");
        if (SI_NO == 0) {
            if (edicion.toNumeroEntero(txtsaldoinicial.getText()) < 1) {
                SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas abrir la caja con saldo inicial igual a cero (0)?");
                if (SI_NO == 0) {
                    if (APERTURA_CIERRE_CAJA(0) != false) {
                        edicion.mensajes(2, "CAJA ABIERTA CORRECTAMENTE.");
                    }
                }
            } else if (APERTURA_CIERRE_CAJA(0) != false) {
                edicion.mensajes(2, "CAJA ABIERTA CORRECTAMENTE.");
            }
        }
    }//GEN-LAST:event_btnabrircajaActionPerformed

    private void btncerrarcajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrarcajaActionPerformed
        // TODO add your handling code here:
        if (TB_BILLETES.getRowCount() > 0 | TB_MONEDAS.getRowCount() > 0) {
            if (APERTURA_CIERRE_CAJA(1) != false) {
                edicion.mensajes(2, "CAJA CERRADA CORRECTAMENTE.");
            }
        } else {
            edicion.mensajes(1, "por favor debe inventariar la caja registrado antes de hacer el cierre.");
        }

    }//GEN-LAST:event_btncerrarcajaActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        NUEVA_APERTURA();
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JD_apertura;
    private com.toedter.calendar.JDateChooser JD_cierre;
    private javax.swing.JMenuItem JM_ELIMINABILLETES;
    private javax.swing.JMenuItem JM_ELIMINAMONEDAS;
    private javax.swing.JLabel LBCNTBILLETE;
    private javax.swing.JLabel LBCNTMONEDA;
    private javax.swing.JLabel LB_BILLETE;
    private javax.swing.JLabel LB_ESTADO_CAJA;
    private javax.swing.JLabel LB_MONEDA;
    private javax.swing.JPopupMenu POPUPBILLETES;
    private javax.swing.JPopupMenu POPUPMONEDAS;
    private javax.swing.JTable TB_BILLETES;
    private javax.swing.JTable TB_MONEDAS;
    private javax.swing.JButton btnabrircaja;
    private javax.swing.JButton btncerrarcaja;
    private javax.swing.JComboBox<String> combobilletes;
    private javax.swing.JComboBox<String> combomonedas;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtcntbillete;
    private javax.swing.JTextField txtcntmoneda;
    private javax.swing.JTextField txtnumcaja;
    private javax.swing.JTextField txtsaldoinicial;
    private javax.swing.JTextField txttotalbilletes;
    private javax.swing.JTextField txttotalcaja;
    private javax.swing.JTextField txttotalmonedas;
    // End of variables declaration//GEN-END:variables

    private void llenarcombos() {
        combobilletes.removeAllItems();
        combobilletes.addItem(null);
        combomonedas.removeAllItems();
        combomonedas.addItem(null);
        for (Object[] objects : cajaDao.DIVISAS_BILLETES()) {
            combobilletes.addItem(formatoTexto.numerico(objects[0].toString()));
        }
        for (Object[] objects : cajaDao.DIVISAS_MONEDAS()) {
            combomonedas.addItem(objects[0].toString());
        }
        txtnumcaja.setText(cajaDao.NUMERO_CAJA());
        DEFINIR_ESTADO_CAJA();
    }

    private void DEFINIR_ESTADO_CAJA() {
        caja = cajaDao.SELECT_CAJA("1," + txtnumcaja.getText());
        if (caja != null) {
            if (caja.getEstadoCaja() == 1) {
                LB_ESTADO_CAJA.setText("CAJA ABIERTA");
                LB_ESTADO_CAJA.setBackground(Color.BLUE);
                LB_ESTADO_CAJA.setOpaque(true);
                LB_ESTADO_CAJA.setForeground(Color.WHITE);
                txtsaldoinicial.setText("$ " + formatoTexto.numerico(caja.getSaldoInicial()));
                txtsaldoinicial.setEnabled(false);
                btnabrircaja.setEnabled(false);
                JD_apertura.setDate(caja.getFechahoraapertura());
            } else {
                LB_ESTADO_CAJA.setText("CAJA CERRADA");
                LB_ESTADO_CAJA.setBackground(Color.red);
                LB_ESTADO_CAJA.setOpaque(true);
                LB_ESTADO_CAJA.setForeground(Color.WHITE);
                txtsaldoinicial.setText("$ " + formatoTexto.numerico(caja.getSaldoInicial()));
                txtsaldoinicial.setEnabled(false);
                btnabrircaja.setEnabled(false);
                txtcntbillete.setEnabled(false);
                txtcntmoneda.setEnabled(false);
                JD_cierre.setDate(caja.getFechahoracierre());
            }
            TOTAL_NETO_CAJA();
        }

    }

    private void CARGAR_IMAGEN_DIVISAS(int opcion) {
        if (opcion == 0) {
            Object billetes = combobilletes.getSelectedItem();
            if (billetes != null | billetes != "") {
                ImageIcon divisa = new ImageIcon("C://ThaliSotf Report/img/divisas/B" + billetes + ".jpg");
                LB_BILLETE.setIcon(divisa);
                txtcntbillete.selectAll();
                txtcntbillete.requestFocus();
            }
        }

        if (opcion == 1) {
            Object monedas = combomonedas.getSelectedItem();
            if (monedas != null | monedas != "") {
                ImageIcon divisa = new ImageIcon("C://ThaliSotf Report/img/divisas/M" + monedas + ".jpg");
                LB_MONEDA.setIcon(divisa);
                txtcntmoneda.selectAll();
                txtcntmoneda.requestFocus();
            }
        }

    }

    private void LLENAR_TB_BILLETES() {
        int cntbillete = edicion.toNumeroEntero(txtcntbillete.getText());
        int valorDivisa = edicion.toNumeroEntero(combobilletes.getSelectedItem().toString());
        int total = cntbillete * valorDivisa;
        Object[] databillete = {txtnumcaja.getText(), valorDivisa, cntbillete, total};
        DefaultTableModel model = (DefaultTableModel) TB_BILLETES.getModel();
        model.addRow(databillete);
    }

    boolean validacionBilletes() {
        if (combobilletes.getSelectedItem() == null | combobilletes.getSelectedItem() == "") {
            edicion.mensajes(1, "selecciona el billete a inventariar.");
            return false;
        }

        for (int i = 0; i < TB_BILLETES.getRowCount(); i++) {
            Object billetes = TB_BILLETES.getValueAt(i, 1);
            if (billetes.equals(edicion.toNumeroEntero(combobilletes.getSelectedItem().toString()))) {
                edicion.mensajes(1, "el billete seleccionado se encuentra inventariado.");
                return false;
            }
        }

        if (edicion.toNumeroEntero(txtcntbillete.getText()) < 1) {
            edicion.mensajes(1, "la cantidad de billetes de " + combobilletes.getSelectedItem() + " debe ser mayor a cero (0).");
            return false;
        }
        return true;
    }

    private void LLENAR_TB_MONEDAS() {
        int cntmoneda = edicion.toNumeroEntero(txtcntmoneda.getText());
        int valorDivisa = edicion.toNumeroEntero(combomonedas.getSelectedItem().toString());
        int total = cntmoneda * valorDivisa;
        Object[] datamonedas = {txtnumcaja.getText(), valorDivisa, cntmoneda, total};
        DefaultTableModel model = (DefaultTableModel) TB_MONEDAS.getModel();
        model.addRow(datamonedas);
    }

    boolean validacionMonedas() {
        if (combomonedas.getSelectedItem() == null | combomonedas.getSelectedItem() == "") {
            edicion.mensajes(1, "selecciona la modena a inventariar.");
            return false;
        }

        for (int i = 0; i < TB_MONEDAS.getRowCount(); i++) {
            Object billetes = TB_MONEDAS.getValueAt(i, 1);
            if (billetes.equals(edicion.toNumeroEntero(combomonedas.getSelectedItem().toString()))) {
                edicion.mensajes(1, "la moneda seleccionado se encuentra inventariada.");
                return false;
            }
        }

        if (edicion.toNumeroEntero(txtcntmoneda.getText()) < 1) {
            edicion.mensajes(1, "la cantidad de billetes de " + combomonedas.getSelectedItem() + " debe ser mayor a cero (0).");
            return false;
        }
        return true;
    }

    void TOTAL_NETO_CAJA() {
        edicion.calcula_total(TB_BILLETES, LBCNTBILLETE, txttotalbilletes, 3);
        edicion.calcula_total(TB_MONEDAS, LBCNTMONEDA, txttotalmonedas, 3);
        int acumuladoNeto = (edicion.toNumeroEntero(txttotalbilletes.getText())
                + edicion.toNumeroEntero(txttotalmonedas.getText()))
                + edicion.toNumeroEntero(txtsaldoinicial.getText());
        txttotalcaja.setText("$ " + formatoTexto.numerico(acumuladoNeto));
    }

    Object[] CARGAR_DATA_APERTURA(int opcion) {
        Object[] data = new Object[4];
        if (opcion == 0 | opcion == 1 | opcion == 2) {
            data[0] = opcion;
        }
        data[1] = txtnumcaja.getText();
        data[2] = Variables_Gloabales.EMPLEADO.getIdentificacion();
        data[3] = edicion.toNumeroEntero(txtsaldoinicial.getText());
        return data;
    }

    private boolean APERTURA_CIERRE_CAJA(int ABRIR_CERRAR_CAJA) {
        String msj;
        boolean b = false;
        if (ABRIR_CERRAR_CAJA == 0) {
            msj = "abir";
        } else {
            msj = "cerrar";
        }

        b = cajaDao.CRUD_CAJA(CARGAR_DATA_APERTURA(ABRIR_CERRAR_CAJA));
        DEFINIR_ESTADO_CAJA();

        return b;
    }

    private void NUEVA_APERTURA() {
        txtsaldoinicial.setEnabled(true);
        btnabrircaja.setEnabled(true);
        txtsaldoinicial.setText("$ 0");
        txtsaldoinicial.setEnabled(true);
        btnabrircaja.setEnabled(true);
        txtcntbillete.setEnabled(true);
        txtcntmoneda.setEnabled(true);
    }

    private boolean validarCierreCaja() {

        if (edicion.toNumeroEntero(txtsaldoinicial.getText()) < 1) {
            edicion.mensajes(1, "EL VALOR DEL SALDO INICIAL EN CAJA DEBE SER MAYOR A CERO.");
            return false;
        }

        return true;
    }
}
