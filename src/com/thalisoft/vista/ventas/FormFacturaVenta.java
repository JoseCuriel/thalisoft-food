package com.thalisoft.vista.ventas;

import com.thalisoft.controller.index.ControllerContenedor;
import com.thalisoft.main.util.CambiaFormatoTexto;
import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.Edicion;
import com.thalisoft.main.util.Variables_Gloabales;
import com.thalisoft.main.util.report.Manager_Report;
import com.thalisoft.model.maestros.cliente.Cliente;
import com.thalisoft.model.maestros.producto.Producto;
import com.thalisoft.model.maestros.producto.ProductoDao;
import com.thalisoft.model.maestros.cliente.ClienteDao;
import com.thalisoft.model.preventa.ordenpedido.OrdenPedido;
import com.thalisoft.model.preventa.ordenpedido.OrdenPedidoDao;
import com.thalisoft.model.preventa.plansepare.PlanSepare;
import com.thalisoft.model.preventa.plansepare.PlanSepareDao;
import com.thalisoft.model.venta.FacturaVenta;
import com.thalisoft.model.venta.FacturaVentaDao;
import com.thalisoft.vista.maestros.cliente.FormCliente;
import com.thalisoft.vista.maestros.cliente.FormListarClientes;
import com.thalisoft.vista.maestros.producto.FormListaProductos;
import com.thalisoft.vista.maestros.producto.FormProducto;
import com.thalisoft.vista.preventa.ordenpedido.pagos.cliente.FormPagosCliente;
import com.thalisoft.vista.preventa.plansepare.pagos.FormPagosPlanSepare;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

public class FormFacturaVenta extends javax.swing.JInternalFrame {

    Edicion edicion = new Edicion();
    CambiaFormatoTexto formatoTexto = new CambiaFormatoTexto();
    Manager_Report report = new Manager_Report();
    ProductoDao productoDao;
    FacturaVenta facturaVenta;
    FacturaVentaDao factVentaDao;
    ClienteDao clienteDao;
    PlanSepareDao planDao;
    OrdenPedidoDao pedidoDao;

    public FormFacturaVenta() {
        clienteDao = new ClienteDao();
        facturaVenta = new FacturaVenta();
        factVentaDao = new FacturaVentaDao();
        productoDao = new ProductoDao();
        planDao = new PlanSepareDao();
        pedidoDao = new OrdenPedidoDao();
        initComponents();
        JDateFactura.setDate(DateUtil.newDateTime());
        LBTIPOFAC.setVisible(false);
        TXTTIPOFACT.setVisible(Boolean.FALSE);
        accionesfml();
        txtnumfactura.setText(factVentaDao.NUMERO_FACTURA_VENTA());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        JM_ELIMINAR = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup_mediopago = new javax.swing.ButtonGroup();
        groupimpresion = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnumfactura = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JDateFactura = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        RadioArticulo = new javax.swing.JRadioButton();
        RadioOrden = new javax.swing.JRadioButton();
        RadioPlan = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtidentificacion = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txttelefono = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        JbuttonCliente = new javax.swing.JButton();
        txtnombreape = new javax.swing.JTextField();
        LBTIPOFAC = new javax.swing.JLabel();
        TXTTIPOFACT = new javax.swing.JTextField();
        RadioTiket = new javax.swing.JRadioButton();
        RadioMembrete = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        LB_ESTADOFACTURA = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txtsubtotal = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        txtmontotarjeta = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtvalorsaldo = new javax.swing.JTextField();
        txtpagoacumulado = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtdevuelta = new javax.swing.JTextField();
        txtefectivoencaja = new javax.swing.JTextField();
        txtmontoefectivo = new javax.swing.JTextField();
        txtnumrecibo = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        numeroCopiasFact = new javax.swing.JSpinner();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtreferencia = new javax.swing.JTextField();
        comboproducto = new javax.swing.JComboBox<String>();
        txtcantidad = new javax.swing.JTextField();
        txtprecioventa = new javax.swing.JTextField();
        txtventatotal = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        TB_detalle = new javax.swing.JTable();
        txtstock = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        lb_item = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem6 = new javax.swing.JMenuItem();

        JM_ELIMINAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/cerrar-icono-6397-32.png"))); // NOI18N
        JM_ELIMINAR.setText("ELIMINAR");
        JM_ELIMINAR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JM_ELIMINARActionPerformed(evt);
            }
        });
        jPopupMenu1.add(JM_ELIMINAR);

        setClosable(true);
        setForeground(java.awt.Color.orange);
        setIconifiable(true);
        setTitle("Factura de Venta");

        jPanel1.setBackground(new java.awt.Color(102, 166, 227));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 255)), "FACTURA DE VENTA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Arial Narrow", 1, 24), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("FACTURA No.");

        txtnumfactura.setEditable(false);
        txtnumfactura.setFont(new java.awt.Font("Arial Narrow", 1, 24)); // NOI18N
        txtnumfactura.setHorizontalAlignment(javax.swing.JTextField.TRAILING);

        jLabel2.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("FECHA FACTURA");

        JDateFactura.setDateFormatString("EEE, dd MMMM yyyy");

        jLabel4.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("QUE DESEAS FACTURAR?");

        buttonGroup1.add(RadioArticulo);
        RadioArticulo.setSelected(true);
        RadioArticulo.setText("ARTICULO");
        RadioArticulo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                RadioArticuloItemStateChanged(evt);
            }
        });

        buttonGroup1.add(RadioOrden);
        RadioOrden.setText("ORDEN DE PEDIDO");
        RadioOrden.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                RadioOrdenItemStateChanged(evt);
            }
        });

        buttonGroup1.add(RadioPlan);
        RadioPlan.setText("PLAN SEPARE");
        RadioPlan.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                RadioPlanItemStateChanged(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CLIENTE", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel18.setText("No. DE IDENTIFICACION");

        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("TELFONOS");

        txtidentificacion.setFont(new java.awt.Font("Arial Narrow", 0, 11)); // NOI18N
        txtidentificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidentificacionActionPerformed(evt);
            }
        });

        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("NOMBRES Y APELLIDOS");

        txttelefono.setEditable(false);

        jLabel21.setText("DIRECCIÓN");

        txtdireccion.setEditable(false);

        JbuttonCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/netvibes-user-icono-4355-48.png"))); // NOI18N
        JbuttonCliente.setToolTipText("Consulta Cliente");
        JbuttonCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JbuttonClienteActionPerformed(evt);
            }
        });

        txtnombreape.setEditable(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(96, 96, 96)
                                        .addComponent(jLabel21)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(JbuttonCliente))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(txtidentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtnombreape, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtidentificacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnombreape, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txttelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtdireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(JbuttonCliente))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        LBTIPOFAC.setBackground(new java.awt.Color(255, 153, 51));
        LBTIPOFAC.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        LBTIPOFAC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LBTIPOFAC.setOpaque(true);

        TXTTIPOFACT.setToolTipText("PRESIONA \"ENTER\" PARA REALIZAR LA BUSQUEDA");
        TXTTIPOFACT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TXTTIPOFACTActionPerformed(evt);
            }
        });

        RadioTiket.setBackground(new java.awt.Color(255, 255, 0));
        groupimpresion.add(RadioTiket);
        RadioTiket.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        RadioTiket.setSelected(true);
        RadioTiket.setText("TICKET");
        RadioTiket.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/printer_post.png"))); // NOI18N
        RadioTiket.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        RadioMembrete.setBackground(new java.awt.Color(0, 255, 204));
        groupimpresion.add(RadioMembrete);
        RadioMembrete.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        RadioMembrete.setText("MEMBRETE");

        jLabel5.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 0));
        jLabel5.setText("IMPRIMIR FACTURA EN:");

        jToolBar1.setBackground(new java.awt.Color(102, 166, 227));
        jToolBar1.setRollover(true);

        jButton1.setBackground(new java.awt.Color(102, 166, 227));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Back.png"))); // NOI18N
        jButton1.setToolTipText("Factura Anterior");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setBackground(new java.awt.Color(102, 166, 227));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Next.png"))); // NOI18N
        jButton2.setToolTipText("Factura Siguiente");
        jButton2.setFocusable(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        jLabel16.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("ESTADO DE LA FACTURA:");

        LB_ESTADOFACTURA.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        LB_ESTADOFACTURA.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel11.setBackground(new java.awt.Color(255, 255, 0));
        jLabel11.setFont(new java.awt.Font("Arial Narrow", 0, 30)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("TOTAL A PAGAR");
        jLabel11.setOpaque(true);

        txtsubtotal.setEditable(false);
        txtsubtotal.setBackground(new java.awt.Color(255, 255, 0));
        txtsubtotal.setFont(new java.awt.Font("Arial Narrow", 0, 48)); // NOI18N
        txtsubtotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtsubtotal.setText("$ 0");
        txtsubtotal.setBorder(null);

        jLabel25.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        jLabel25.setText("$ TARJETA");

        txtmontotarjeta.setBackground(new java.awt.Color(204, 153, 0));
        txtmontotarjeta.setFont(new java.awt.Font("Arial Narrow", 0, 20)); // NOI18N
        txtmontotarjeta.setForeground(new java.awt.Color(255, 255, 255));
        txtmontotarjeta.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtmontotarjeta.setText("$ 0");
        txtmontotarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmontotarjetaActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        jLabel12.setText("PAGO ACUMULADO");

        jLabel13.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        jLabel13.setText("SALDO A PAGAR");

        txtvalorsaldo.setEditable(false);
        txtvalorsaldo.setBackground(new java.awt.Color(108, 128, 66));
        txtvalorsaldo.setFont(new java.awt.Font("Arial Narrow", 0, 16)); // NOI18N
        txtvalorsaldo.setForeground(new java.awt.Color(255, 255, 255));
        txtvalorsaldo.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtvalorsaldo.setText("$ 0");

        txtpagoacumulado.setEditable(false);
        txtpagoacumulado.setBackground(new java.awt.Color(214, 214, 182));
        txtpagoacumulado.setFont(new java.awt.Font("Arial Narrow", 0, 16)); // NOI18N
        txtpagoacumulado.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtpagoacumulado.setText("$ 0");

        jLabel23.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        jLabel23.setText("No. RECIBO");

        jLabel17.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        jLabel17.setText("MONTO EN EFECTIVO");

        jLabel22.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        jLabel22.setText("EFECTIVO EN CAJA");

        jLabel24.setFont(new java.awt.Font("Arial Narrow", 1, 12)); // NOI18N
        jLabel24.setText("CAMBIO");

        txtdevuelta.setEditable(false);
        txtdevuelta.setBackground(new java.awt.Color(240, 143, 81));
        txtdevuelta.setFont(new java.awt.Font("Arial Narrow", 0, 16)); // NOI18N
        txtdevuelta.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtdevuelta.setText("$ 0");

        txtefectivoencaja.setEditable(false);
        txtefectivoencaja.setBackground(new java.awt.Color(51, 51, 255));
        txtefectivoencaja.setFont(new java.awt.Font("Arial Narrow", 0, 20)); // NOI18N
        txtefectivoencaja.setForeground(new java.awt.Color(255, 255, 255));
        txtefectivoencaja.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtefectivoencaja.setText("$ 0");

        txtmontoefectivo.setBackground(new java.awt.Color(0, 165, 179));
        txtmontoefectivo.setFont(new java.awt.Font("Arial Narrow", 0, 20)); // NOI18N
        txtmontoefectivo.setForeground(new java.awt.Color(255, 255, 255));
        txtmontoefectivo.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtmontoefectivo.setText("$ 0");

        txtnumrecibo.setFont(new java.awt.Font("Arial Narrow", 0, 16)); // NOI18N
        txtnumrecibo.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtnumrecibo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumreciboActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(27, 27, 27)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtvalorsaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtdevuelta, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel22)))
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtmontoefectivo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtefectivoencaja, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtnumrecibo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtmontotarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(14, 14, 14)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(txtpagoacumulado, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel25))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(txtsubtotal))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                    .addComponent(txtsubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtpagoacumulado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(txtmontotarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(txtvalorsaldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(9, 9, 9)
                                        .addComponent(txtdevuelta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(31, 31, 31)
                                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(txtnumrecibo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtmontoefectivo)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtefectivoencaja, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jLabel26.setText("# DE COPIAS DE LA FACTURA");

        numeroCopiasFact.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        numeroCopiasFact.setModel(new javax.swing.SpinnerNumberModel(1, 1, 4, 1));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtnumfactura, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(JDateFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(17, 17, 17))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(RadioArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(20, 20, 20)
                                        .addComponent(RadioOrden, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(RadioPlan)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LBTIPOFAC, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(TXTTIPOFACT, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(LB_ESTADOFACTURA, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(RadioTiket, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(RadioMembrete))
                            .addComponent(numeroCopiasFact, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtnumfactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(RadioPlan, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(RadioOrden)
                                    .addComponent(RadioArticulo)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(LB_ESTADOFACTURA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(JDateFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(LBTIPOFAC, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TXTTIPOFACT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addComponent(numeroCopiasFact))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(RadioTiket, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(RadioMembrete)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(212, 193, 114));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "DETALLE VENTA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Narrow", 0, 18))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        jLabel6.setText("REFERENCIA");

        jLabel7.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        jLabel7.setText("DESCRIPCION");

        jLabel8.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        jLabel8.setText("CANTIDAD");

        jLabel9.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        jLabel9.setText("PRECIO UNIDAD");

        jLabel10.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("PRECIO TOTAL");

        txtreferencia.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        txtreferencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtreferenciaActionPerformed(evt);
            }
        });

        comboproducto.setEditable(true);
        comboproducto.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N

        txtcantidad.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        txtcantidad.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtcantidad.setText("0");
        txtcantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcantidadActionPerformed(evt);
            }
        });

        txtprecioventa.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        txtprecioventa.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtprecioventa.setText("$ 0");
        txtprecioventa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtprecioventaActionPerformed(evt);
            }
        });

        txtventatotal.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        txtventatotal.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtventatotal.setText("$ 0");

        TB_detalle.setBackground(new java.awt.Color(204, 255, 204));
        TB_detalle.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        TB_detalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Id. Detalle", "Referencia", "Descripcion", "Cantidad", "Precio Unidad", "Precio Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TB_detalle.setComponentPopupMenu(jPopupMenu1);
        TB_detalle.setRowHeight(22);
        TB_detalle.setSelectionBackground(new java.awt.Color(255, 255, 102));
        TB_detalle.setSelectionForeground(new java.awt.Color(0, 0, 0));
        TB_detalle.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(TB_detalle);
        if (TB_detalle.getColumnModel().getColumnCount() > 0) {
            TB_detalle.getColumnModel().getColumn(0).setMaxWidth(50);
            TB_detalle.getColumnModel().getColumn(1).setMaxWidth(70);
            TB_detalle.getColumnModel().getColumn(2).setMinWidth(400);
        }

        txtstock.setEditable(false);
        txtstock.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        txtstock.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtstock.setText("0");

        jLabel15.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        jLabel15.setText("STOCK ACTUAL");

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/vista_style_business_and_data_icons_icons_pack_120673/canasto-icono-6453-48.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        jLabel14.setText("Item Agregados:");

        lb_item.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        lb_item.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_item.setText("0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addGap(1, 1, 1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 28, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtreferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(comboproducto, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(183, 183, 183)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtstock, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtprecioventa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtventatotal)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(436, 436, 436)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lb_item, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtstock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtprecioventa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtventatotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtcantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtreferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboproducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lb_item, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jMenu1.setBackground(new java.awt.Color(153, 255, 255));
        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/userconfig-icono-8183-32.png"))); // NOI18N
        jMenu1.setText("Gestion");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/agregar-nuevo-documento-de-archivo-mas-icono-6249-32.png"))); // NOI18N
        jMenuItem2.setText("Nuevo");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/insert-coin.png"))); // NOI18N
        jMenuItem4.setText("Facturar");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/printer.png"))); // NOI18N
        jMenuItem5.setText("Imprimir Factura");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F3, 0));
        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/view-employed.png"))); // NOI18N
        jMenuItem7.setText("Consultar");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem7);
        jMenu1.add(jSeparator2);

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/thalisoft/image/iconos/exit.png"))); // NOI18N
        jMenuItem6.setText("Salir");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem6);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RadioArticuloItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_RadioArticuloItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            LBTIPOFAC.setVisible(false);
            TXTTIPOFACT.setVisible(false);
        }
    }//GEN-LAST:event_RadioArticuloItemStateChanged

    private void RadioOrdenItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_RadioOrdenItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            LBTIPOFAC.setVisible(Boolean.TRUE);
            TXTTIPOFACT.setVisible(Boolean.TRUE);
            LBTIPOFAC.setText("No. " + RadioOrden.getText());
            TXTTIPOFACT.grabFocus();
            TXTTIPOFACT.setText(null);
            LIMPIAR_FACTURA_VENTA(1);
        }
    }//GEN-LAST:event_RadioOrdenItemStateChanged

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void JM_ELIMINARActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JM_ELIMINARActionPerformed
        // TODO add your handling code here:
        try {
            int SI_NO = (int) edicion.msjQuest(1, "estas seguro que deseas eliminar el producto?");
            if (SI_NO == 0) {
                int idcompra = Integer.parseInt(TB_detalle.getValueAt(TB_detalle.getSelectedRow(), 0).toString());
                if (factVentaDao.BORRAR_PRODUCTO_DETALLE(idcompra) != false) {
                    edicion.menu_emergente(TB_detalle);
                    calculatotalesfactura();
                }
            }
        } catch (Exception e) {
            edicion.mensajes(3, "no haz seleccionado algun producto.");
        }
    }//GEN-LAST:event_JM_ELIMINARActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        IMPRIMIR_FACTURA();
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
        FACTURAR_FACTURA_VENTA();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void RadioPlanItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_RadioPlanItemStateChanged
        // TODO add your handling code here:
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            LBTIPOFAC.setVisible(Boolean.TRUE);
            TXTTIPOFACT.setVisible(Boolean.TRUE);
            LBTIPOFAC.setText("No. " + RadioPlan.getText());
            TXTTIPOFACT.setText(null);
            TXTTIPOFACT.grabFocus();
            LIMPIAR_FACTURA_VENTA(1);
        }

    }//GEN-LAST:event_RadioPlanItemStateChanged

    private void txtidentificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidentificacionActionPerformed
        CARGAR_CLIENTE(txtidentificacion.getText());
    }//GEN-LAST:event_txtidentificacionActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        Object IDFACTURA = edicion.msjQuest(2, "INGRESA EL # DE LA FACTURA.");
        CARGAR_FACTURA_VENTA(IDFACTURA);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void TXTTIPOFACTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TXTTIPOFACTActionPerformed
        // TODO add your handling code here:
        if (RadioPlan.isSelected() | RadioOrden.isSelected()) {
            llenar_PRODUCTO();
            llenar_cliente();
        }

    }//GEN-LAST:event_TXTTIPOFACTActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        LIMPIAR_FACTURA_VENTA(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void txtprecioventaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtprecioventaActionPerformed
        // TODO add your handling code here:
        if (VALIDAR_FORMULARIO() != false) {
            if (factVentaDao.CRUD_VENTA(DATOS_FACTURA(0)) != false) {
                CARGAR_DETALLE();
                LIMPIAR_FACTURA_VENTA(1);
                txtreferencia.grabFocus();
            }
        }
    }//GEN-LAST:event_txtprecioventaActionPerformed

    private void txtcantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcantidadActionPerformed
        // TODO add your handling code here:
        txtprecioventa.selectAll();
        txtprecioventa.requestFocus();
    }//GEN-LAST:event_txtcantidadActionPerformed

    private void txtreferenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtreferenciaActionPerformed
        // TODO add your handling code here:
        CARGAR_PRODUCTO(txtreferencia.getText());
        if (VALIDAR_FORMULARIO() != false) {
            if (factVentaDao.CRUD_VENTA(DATOS_FACTURA(0)) != false) {
                CARGAR_DETALLE();
                LIMPIAR_FACTURA_VENTA(1);
                txtreferencia.grabFocus();
            }
        }
    }//GEN-LAST:event_txtreferenciaActionPerformed

    private void JbuttonClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JbuttonClienteActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormListarClientes.class.getName());
        if (ji == null || ji.isClosed()) {
            ji = new FormListarClientes(this, null, null, null);
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormListarClientes.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormListarClientes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_JbuttonClienteActionPerformed

    private void txtmontotarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmontotarjetaActionPerformed
        // TODO add your handling code here:
        txtnumrecibo.grabFocus();
    }//GEN-LAST:event_txtmontotarjetaActionPerformed

    private void txtnumreciboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumreciboActionPerformed
        // TODO add your handling code here:
        txtmontoefectivo.selectAll();
        txtmontoefectivo.requestFocus();
    }//GEN-LAST:event_txtnumreciboActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        BUSCAR_FACTURA(0);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        BUSCAR_FACTURA(1);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        JInternalFrame ji = validador.getJInternalFrame(FormListaProductos.class.getName());
        if (ji == null || ji.isClosed()) {
            ji = new FormListaProductos(this, null, null, null, null);
            ControllerContenedor.getjDesktopPane1().add(ji, 0);
            validador.addJIframe(FormListaProductos.class.getName(), ji);
            ji.setVisible(true);
        } else {
            ji.show(true);
            try {
                ji.setIcon(false);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(FormListaProductos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser JDateFactura;
    private javax.swing.JMenuItem JM_ELIMINAR;
    private javax.swing.JButton JbuttonCliente;
    private javax.swing.JLabel LBTIPOFAC;
    private javax.swing.JLabel LB_ESTADOFACTURA;
    private javax.swing.JRadioButton RadioArticulo;
    private javax.swing.JRadioButton RadioMembrete;
    private javax.swing.JRadioButton RadioOrden;
    private javax.swing.JRadioButton RadioPlan;
    private javax.swing.JRadioButton RadioTiket;
    private javax.swing.JTable TB_detalle;
    private javax.swing.JTextField TXTTIPOFACT;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup_mediopago;
    public javax.swing.JComboBox<String> comboproducto;
    private javax.swing.ButtonGroup groupimpresion;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lb_item;
    private javax.swing.JSpinner numeroCopiasFact;
    public javax.swing.JTextField txtcantidad;
    private javax.swing.JTextField txtdevuelta;
    public static javax.swing.JTextField txtdireccion;
    private javax.swing.JTextField txtefectivoencaja;
    public static javax.swing.JTextField txtidentificacion;
    private javax.swing.JTextField txtmontoefectivo;
    private javax.swing.JTextField txtmontotarjeta;
    public static javax.swing.JTextField txtnombreape;
    private javax.swing.JTextField txtnumfactura;
    private javax.swing.JTextField txtnumrecibo;
    private javax.swing.JTextField txtpagoacumulado;
    public javax.swing.JTextField txtprecioventa;
    public javax.swing.JTextField txtreferencia;
    public javax.swing.JTextField txtstock;
    private javax.swing.JTextField txtsubtotal;
    public static javax.swing.JTextField txttelefono;
    private javax.swing.JTextField txtvalorsaldo;
    private javax.swing.JTextField txtventatotal;
    // End of variables declaration//GEN-END:variables

    private boolean VALIDAR_FORMULARIO() {
        if (txtidentificacion.getText().isEmpty() | txtidentificacion.getText() == null) {
            edicion.mensajes(1, "por favor selecciona un cliente.");
            JbuttonCliente.grabFocus();
            return false;
        }
        if (txtnumfactura.getText().isEmpty() | txtnumfactura.getText() == null) {
            edicion.mensajes(1, "ingrese el numero de factura.");
            return false;
        }

        if (JDateFactura.getDate() == null) {
            edicion.mensajes(1, "selecciona la fecha de la factura.");
            return false;
        }

        if (txtreferencia.getText() == null) {
            edicion.mensajes(1, "por favor selecciona el producto.");
            return false;
        }

        if (edicion.toNumeroEntero(txtcantidad.getText()) < 1) {
            edicion.mensajes(1, "la cantidad debe ser mayor a cero (0).");
            return false;
        }
        if (edicion.toNumeroEntero(txtcantidad.getText()) > edicion.toNumeroEntero(txtstock.getText())) {
            edicion.mensajes(1, "la cantidad debe ser menor o igual al stock actual.");
            return false;
        }

        return true;
    }

    private void accionesfml() {

        comboproducto.getEditor().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                CARGAR_PRODUCTO(comboproducto.getSelectedItem().toString());

            }

        });

        TXTTIPOFACT.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                TXTTIPOFACT.setText(edicion.AGREGAR_CEROS_LEFT(
                        edicion.toNumeroEntero(TXTTIPOFACT.getText())));
            }
        });

        txtcantidad.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                CALCULARTOTALVENTA();
            }
        });
        txtmontoefectivo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                txtmontoefectivo.setText("$ " + formatoTexto.numerico(
                        edicion.toNumeroEntero(txtmontoefectivo.getText())));
                ACUMULARPAGO();

            }

        });
        txtprecioventa.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                CALCULARTOTALVENTA();
            }
        });

        txtmontotarjeta.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
                /*if (txtmontotarjeta.getText().length() == txtvalorsaldo.getText().length()) {
                 e.consume();
                 }*/
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                txtmontotarjeta.setText("$ " + formatoTexto.numerico(
                        edicion.toNumeroEntero(txtmontotarjeta.getText())));
                ACUMULARPAGO();
            }

        });

    }

    private void DEVOLVERDINERO() {

        int cambio = ((edicion.toNumeroEntero(txtsubtotal.getText())
                - edicion.toNumeroEntero(txtmontoefectivo.getText()))
                - edicion.toNumeroEntero(txtmontotarjeta.getText())) * (-1);

        int efectivoEncaja = edicion.toNumeroEntero(txtmontoefectivo.getText()) - cambio;

        if (cambio < 0) {
            txtdevuelta.setText("$ " + formatoTexto.numerico(0));
            txtefectivoencaja.setText("$ " + formatoTexto.numerico(0));
        } else {
            txtdevuelta.setText("$ " + formatoTexto.numerico(cambio));
            txtefectivoencaja.setText("$ " + formatoTexto.numerico(efectivoEncaja));
        }
    }

    private void ACUMULARPAGO() {
        int acumularValores = 0, montoTarjeta = edicion.toNumeroEntero(txtmontotarjeta.getText()),
                montoEfectivo = edicion.toNumeroEntero(txtmontoefectivo.getText()), SaldototalApagar = 0;
        if (montoTarjeta > 0 && montoTarjeta <= edicion.toNumeroEntero(txtsubtotal.getText())) {
            acumularValores = acumularValores + montoTarjeta;
        }
        if (montoEfectivo > 0) {
            acumularValores = acumularValores + montoEfectivo;
            DEVOLVERDINERO();
        }
        SaldototalApagar = edicion.toNumeroEntero(txtsubtotal.getText()) - acumularValores;
        txtpagoacumulado.setText("$ " + formatoTexto.numerico(acumularValores));
        if (SaldototalApagar < 0) {
            txtvalorsaldo.setText("$ " + formatoTexto.numerico(0));
        } else {
            txtvalorsaldo.setText("$ " + formatoTexto.numerico(SaldototalApagar));
        }

    }

    private void CALCULARTOTALVENTA() {
        int totalcompra = edicion.toNumeroEntero(txtprecioventa.getText())
                * edicion.toNumeroEntero(txtcantidad.getText());
        txtventatotal.setText("$ " + formatoTexto.numerico(totalcompra));
    }

    private void CARGAR_PRODUCTO(String text) {
//        if (RadioArticulo.isSelected()) {
        Producto producto = productoDao.READ_PRODUCTO(text);
        if (producto != null) {
            txtreferencia.setText(producto.getReferencia());
            comboproducto.setSelectedItem(producto.getDescripcion());
            txtprecioventa.setText("$ " + formatoTexto.numerico(producto.getPrecio_venta()));
            txtstock.setText(formatoTexto.numerico(producto.getStrock()));
            txtcantidad.setText("1");
            txtcantidad.selectAll();
            txtcantidad.requestFocus();

        } else {
            int SI_NO = (int) edicion.msjQuest(1, "el producto no se encuentra registrado, deseas registrarlo?");

            if (SI_NO == 0) {
                JInternalFrame ji = validador.getJInternalFrame(FormProducto.class
                        .getName());
                if (ji == null || ji.isClosed()) {
                    ji = new FormProducto();
                    ControllerContenedor.getjDesktopPane1().add(ji, 0);
                    validador
                            .addJIframe(FormProducto.class
                                    .getName(), ji);
                    ji.setVisible(true);
                } else {
                    ji.show(true);
                    try {
                        ji.setIcon(false);

                    } catch (PropertyVetoException ex) {
                        Logger.getLogger(FormCliente.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        if (RadioOrden.isSelected()) {
            dato_producto();
        }

        if (RadioPlan.isSelected()) {
            dato_producto();
        }
    }

    private Object[] DATOS_FACTURA(int opcion) {
        Object[] datos = new Object[10];
        String NUMEROPLAN = null, NUMEROORDEN = null;
        if (opcion == 0 | opcion == 1 | opcion == 2) {
            datos[0] = opcion;
        }

        if (RadioPlan.isSelected()) {
            NUMEROPLAN = TXTTIPOFACT.getText();
        }
        if (RadioOrden.isSelected()) {
            NUMEROORDEN = TXTTIPOFACT.getText();
        }

        datos[1] = "" + txtnumfactura.getText() + "";
        datos[2] = "'" + NUMEROORDEN + "'";
        datos[3] = "'" + NUMEROPLAN + "'";
        datos[4] = "'" + txtidentificacion.getText() + "'";
        datos[5] = "'" + Variables_Gloabales.EMPLEADO.getIdentificacion() + "'";
        datos[6] = "'" + edicion.formatearFechaSQL(JDateFactura.getDate()) + "'";
        datos[7] = "'" + txtreferencia.getText() + "'";
        datos[8] = edicion.toNumeroEntero(txtcantidad.getText());
        datos[9] = edicion.toNumeroEntero(txtprecioventa.getText());
        return datos;
    }

    private void calculatotalesfactura() {
        edicion.calcula_total(TB_detalle, lb_item, txtsubtotal, 5);
        int saldo = edicion.toNumeroEntero(txtsubtotal.getText()) - edicion.toNumeroEntero(txtpagoacumulado.getText());
        txtvalorsaldo.setText("$ " + formatoTexto.numerico(saldo));
    }

    private void CARGAR_CLIENTE(String text) {
        Cliente cliente = clienteDao.CONSULTAR_CLIENTE(text);
        if (cliente != null) {
            LLENAR_CLIENTE(cliente);
            txtreferencia.grabFocus();
        } else {
            int SI_NO = (int) edicion.msjQuest(1, "el cliente no se encuentra registrado, deseas registrarlo?");

            if (SI_NO == 0) {
                JInternalFrame ji = validador.getJInternalFrame(FormCliente.class
                        .getName());
                if (ji == null || ji.isClosed()) {
                    ji = new FormCliente();
                    ControllerContenedor.getjDesktopPane1().add(ji, 0);
                    validador
                            .addJIframe(FormCliente.class
                                    .getName(), ji);
                    ji.setVisible(true);
                } else {
                    ji.show(true);
                    try {
                        ji.setIcon(false);

                    } catch (PropertyVetoException ex) {
                        Logger.getLogger(FormCliente.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    Object[][] DETALLE_ORDEN, DETALLE_PLAN;

    private void llenar_PRODUCTO() {
        if (RadioPlan.isSelected()) {
            comboproducto.removeAllItems();
            comboproducto.addItem("");
            DETALLE_PLAN = planDao.SELECT_PLANSEPARE("7," + TXTTIPOFACT.getText());
            for (Object[] SELECT_PLANSEPARE : DETALLE_PLAN) {
                comboproducto.addItem(SELECT_PLANSEPARE[1].toString());
            }
        }

        if (RadioOrden.isSelected()) {
            comboproducto.removeAllItems();
            comboproducto.addItem("");
            DETALLE_ORDEN = pedidoDao.DETALLE_ORDEN_COMPRA(TXTTIPOFACT.getText());
            for (Object[] DETALLE_ORDEN_COMPRA : DETALLE_ORDEN) {
                comboproducto.addItem(DETALLE_ORDEN_COMPRA[2].toString());
            }
        }

    }

    private void dato_producto() {
        if (RadioOrden.isSelected()) {
            for (Object[] DETALLE_ORDEN1 : DETALLE_ORDEN) {
                if (comboproducto.getSelectedItem() == DETALLE_ORDEN1[2]) {
                    txtreferencia.setText(DETALLE_ORDEN1[1].toString());
                    txtcantidad.setText(DETALLE_ORDEN1[4].toString());
                    txtprecioventa.setText("$ " + formatoTexto.numerico(DETALLE_ORDEN1[5].toString()));
                    CALCULARTOTALVENTA();
                }
            }
        }

        if (RadioPlan.isSelected()) {
            for (Object[] DETALLE_PLAN1 : DETALLE_PLAN) {
                if (comboproducto.getSelectedItem() == DETALLE_PLAN1[1]) {
                    txtreferencia.setText(DETALLE_PLAN1[0].toString());
                    txtcantidad.setText(DETALLE_PLAN1[2].toString());
                    txtprecioventa.setText("$ " + formatoTexto.numerico(DETALLE_PLAN1[3].toString()));
                    CALCULARTOTALVENTA();
                }
            }
        }

    }

    private void llenar_cliente() {
        if (RadioPlan.isSelected()) {
            PlanSepare ps = planDao.CONSULTAR_PLAN_SEPARE(TXTTIPOFACT.getText());
            if (ps != null) {
                CARGAR_CLIENTE(planDao.CONSULTAR_PLAN_SEPARE(
                        TXTTIPOFACT.getText()).getCliente().getIdentificacion());
                if (ps.getSaldo() > 0) {
                    int SI_NO = (int) edicion.msjQuest(1, "el " + RadioPlan.getText() + " tiene un saldo sin pagar de $ " + formatoTexto.numerico(ps.getSaldo()) + ","
                            + " deseas registrar el pago?");
                    if (SI_NO == 0) {
                        JInternalFrame ji = validador.getJInternalFrame(FormPagosPlanSepare.class
                                .getName());
                        if (ji == null || ji.isClosed()) {
                            ji = new FormPagosPlanSepare();
                            ControllerContenedor.getjDesktopPane1().add(ji, 0);
                            validador
                                    .addJIframe(FormPagosPlanSepare.class
                                            .getName(), ji);
                            ji.setVisible(true);
                        } else {
                            ji.show(true);
                            try {
                                ji.setIcon(false);

                            } catch (PropertyVetoException ex) {
                                Logger.getLogger(FormPagosPlanSepare.class
                                        .getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            } else {
                edicion.mensajes(1, "el " + RadioPlan.getText() + " no se encuentra registrado.");
            }
        }
        if (RadioOrden.isSelected()) {
            try {
                OrdenPedido op = pedidoDao.CONSULTA_ORDEN_COMPRA(TXTTIPOFACT.getText());
                if (op != null) {
                    if (op.getSaldo() > 0) {
                        int SI_NO = (int) edicion.msjQuest(1, "la " + RadioOrden.getText() + " tiene un saldo sin pagar de $ " + formatoTexto.numerico(op.getSaldo()) + ","
                                + " deseas registrar el pago?");

                        if (SI_NO == 0) {
                            JInternalFrame ji = validador.getJInternalFrame(FormPagosCliente.class
                                    .getName());
                            if (ji == null || ji.isClosed()) {
                                ji = new FormPagosCliente();
                                ControllerContenedor.getjDesktopPane1().add(ji, 0);
                                validador
                                        .addJIframe(FormPagosCliente.class
                                                .getName(), ji);
                                ji.setVisible(true);
                            } else {
                                ji.show(true);
                                try {
                                    ji.setIcon(false);

                                } catch (PropertyVetoException ex) {
                                    Logger.getLogger(FormPagosCliente.class
                                            .getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }
                    } else {
                        CARGAR_CLIENTE(pedidoDao.CONSULTA_ORDEN_COMPRA(
                                TXTTIPOFACT.getText()).getCliente().getIdentificacion());

                    }
                } else {
                    edicion.mensajes(1, "la " + RadioOrden.getText() + " no se encuentra registrada.");
                }
            } catch (ParseException ex) {
                Logger.getLogger(FormFacturaVenta.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void LIMPIAR_FACTURA_VENTA(int opcion) {
        if (opcion == 0) {
            txtnumfactura.setText(factVentaDao.NUMERO_FACTURA_VENTA());
            txtidentificacion.setText(null);
            txtnombreape.setText(null);
            txttelefono.setText(null);
            txtdireccion.setText(null);
            TXTTIPOFACT.setText(null);
            RadioArticulo.setSelected(true);
            txtnumrecibo.setText(null);
            txtsubtotal.setText("$ 0");
            txtvalorsaldo.setText("$ 0");
            txtmontoefectivo.setText("$ 0");
            txtmontotarjeta.setText("$ 0");
            txtefectivoencaja.setText("$ 0");
            txtpagoacumulado.setText("$ 0");
            txtdevuelta.setText("$ 0");
            lb_item.setText("0");
            txtreferencia.setText(null);
            comboproducto.setSelectedItem(null);
            txtcantidad.setText("0");
            txtstock.setText("0");
            txtprecioventa.setText("$ 0");
            txtventatotal.setText("$ 0");
            edicion.limpiar_tablas(TB_detalle);
            LB_ESTADOFACTURA.setOpaque(false);
            LB_ESTADOFACTURA.setText(null);
            BLOQUEAR_PANEL_PRODUCTOS(true);
        }

        if (opcion == 1) {
            txtreferencia.setText(null);
            comboproducto.setSelectedItem(null);
            txtcantidad.setText("0");
            txtstock.setText("0");
            txtprecioventa.setText("$ 0");
            txtventatotal.setText("$ 0");
            comboproducto.setSelectedItem(null);
        }

    }

    boolean VALIDAD_FACTURA() {

        if (RadioArticulo.isSelected()) {

            if (edicion.toNumeroEntero(txtpagoacumulado.getText())
                    < edicion.toNumeroEntero(txtsubtotal.getText())) {
                edicion.mensajes(3, "LA CANTIDAD DE DINERO acumulada DEBE SER MAYOR O IGUAL AL TOTAL A PAGAR.");
                return false;
            }
            if (edicion.toNumeroEntero(txtmontotarjeta.getText()) > edicion.toNumeroEntero(txtvalorsaldo.getText())
                    && edicion.toNumeroEntero(txtvalorsaldo.getText()) > 0) {
                edicion.mensajes(1, "el monto pagado en tarjeta excede el saldo actual y debe ser menor o igual al saldo.");
                txtmontotarjeta.grabFocus();
                return false;
            }
            return true;
        }

        if (RadioOrden.isSelected() | RadioPlan.isSelected()) {
            if (TXTTIPOFACT.getText().isEmpty() | TXTTIPOFACT.getText() == null) {
                edicion.mensajes(1, "ingrese el numero de orden de pedido o plan separe");
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    private Object[] DATOS_FACTURAR_VENTA() {
        Object[] datos = new Object[7];
        datos[0] = "" + txtnumfactura.getText() + "";
        datos[1] = "'" + txtnumrecibo.getText() + "'";
        datos[2] = "" + edicion.toNumeroEntero(txtmontoefectivo.getText()) + "";
        datos[3] = "" + edicion.toNumeroEntero(txtdevuelta.getText()) + "";
        datos[4] = "" + edicion.toNumeroEntero(txtefectivoencaja.getText()) + "";
        datos[5] = "" + edicion.toNumeroEntero(txtmontotarjeta.getText()) + "";
        datos[6] = "" + edicion.toNumeroEntero(txtpagoacumulado.getText()) + "";
        return datos;
    }

    private void CARGAR_FACTURA_VENTA(Object IDFACTURA) {
        facturaVenta = factVentaDao.SELECT_VENTA(IDFACTURA);
        if (facturaVenta != null) {
            txtnumfactura.setText(edicion.AGREGAR_CEROS_LEFT(facturaVenta.getNumeroFactura()));
            JDateFactura.setDate(facturaVenta.getFechaFactura());

            if (!facturaVenta.getNumOrdenPedido().equals("null")) {
                RadioOrden.setSelected(true);
                TXTTIPOFACT.setText(facturaVenta.getNumOrdenPedido());
            }

            if (!facturaVenta.getPlansepare().equals("null")) {
                RadioPlan.setSelected(true);
                TXTTIPOFACT.setText(facturaVenta.getPlansepare());
            }

            if (facturaVenta.getNumOrdenPedido().equals("null") && facturaVenta.getPlansepare().equals("null")) {
                RadioArticulo.setSelected(true);
                TXTTIPOFACT.setText(null);
            }

            txtpagoacumulado.setText("$ " + formatoTexto.numerico(facturaVenta.getPagoacumulado()));
            txtmontotarjeta.setText("$ " + formatoTexto.numerico(facturaVenta.getMontoTarjeta()));
            txtnumrecibo.setText(facturaVenta.getNumerorecibo());
            txtmontoefectivo.setText("$ " + formatoTexto.numerico(facturaVenta.getCntrecibida()));
            txtefectivoencaja.setText("$ " + formatoTexto.numerico(facturaVenta.getMontoEfectivo()));
            txtdevuelta.setText("$ " + formatoTexto.numerico(facturaVenta.getCntdevuelta()));
            LLENAR_CLIENTE(facturaVenta.getCliente());
            CARGAR_DETALLE();

            if (facturaVenta.getEstadoFactura() == 1) {
                BLOQUEAR_PANEL_PRODUCTOS(false);
            } else {
                BLOQUEAR_PANEL_PRODUCTOS(true);
            }
            ESTADO_FACTURA();

            int SaldototalApagar = edicion.toNumeroEntero(txtsubtotal.getText())
                    - edicion.toNumeroEntero(txtpagoacumulado.getText());
            txtpagoacumulado.setText("$ " + formatoTexto.numerico(edicion.toNumeroEntero(txtpagoacumulado.getText())));
            if (SaldototalApagar < 0) {
                txtvalorsaldo.setText("$ " + formatoTexto.numerico(0));
            } else {
                txtvalorsaldo.setText("$ " + formatoTexto.numerico(SaldototalApagar));
            }
        } else {
            edicion.mensajes(1, "la factura aun no hacido registrada.");
        }
    }

    private void CARGAR_DETALLE() {
        edicion.llenarTabla(TB_detalle,
                factVentaDao.SELECT_DETALLEVENTA(txtnumfactura.getText()));
        calculatotalesfactura();
    }

    private void FACTURAR_FACTURA_VENTA() {
        if (VALIDAD_FACTURA() != false) {
            facturaVenta = factVentaDao.SELECT_VENTA(txtnumfactura.getText());
            if (facturaVenta != null) {
                if (facturaVenta.getEstadoFactura() == 0) {
                    if (factVentaDao.FACTURAR_FACTURA(DATOS_FACTURAR_VENTA()) != false) {
                        int sino = (int) edicion.msjQuest(1, "deseas imprimir la factura.");
                        if (sino == 0) {
                            IMPRIMIR_FACTURA();
                        }
                        BLOQUEAR_PANEL_PRODUCTOS(false);
                        ESTADO_FACTURA();
                    }

                } else {
                    edicion.mensajes(3, "la factura se encuentra facturada. por tal razon no podra realizar mas cambios."
                            + "\nle recomendamos crear una nueva factura presionando 'F2'.");
                    BLOQUEAR_PANEL_PRODUCTOS(false);
                }
            } else {
                edicion.mensajes(1, "la factura aun no se encuentra registrada.");
            }
        }
    }

    private void IMPRIMIR_FACTURA() {
        facturaVenta = factVentaDao.SELECT_VENTA(txtnumfactura.getText());
        if (facturaVenta != null) {
            if (facturaVenta.getEstadoFactura() != 0) {
                if (RadioTiket.isSelected()) {
                    report.FACTURA_VENTA_TIKECT(txtnumfactura.getText(),
                            edicion.toNumeroEntero(numeroCopiasFact.getValue().toString()));
                }
                if (RadioMembrete.isSelected()) {
                    report.FACTURA_VENTA_MEMBRETE(txtnumfactura.getText());
                }
            } else {
                edicion.mensajes(1, "la venta aun no hacido facturada por favor"
                        + " seleccione en el menu gestion la opcion facturar o PRESIONA 'F5'.");
            }
        } else {
            edicion.mensajes(1, "la factura aun no hacido registrada.");
        }

    }

    private void BLOQUEAR_PANEL_PRODUCTOS(boolean opcion) {
        txtreferencia.setEnabled(opcion);
        comboproducto.setEnabled(opcion);
        txtcantidad.setEnabled(opcion);
        txtprecioventa.setEnabled(opcion);
        JM_ELIMINAR.setEnabled(opcion);
    }

    private void ESTADO_FACTURA() {
        LB_ESTADOFACTURA.setOpaque(true);
        if (facturaVenta != null) {
            if (facturaVenta.getEstadoFactura() == 0) {
                LB_ESTADOFACTURA.setText("VENTA SIN FACTURAR");
                LB_ESTADOFACTURA.setForeground(Color.WHITE);
                LB_ESTADOFACTURA.setBackground(Color.red);
            } else {
                LB_ESTADOFACTURA.setText("VENTA FACTURADA");
                LB_ESTADOFACTURA.setForeground(Color.BLACK);
                LB_ESTADOFACTURA.setBackground(Color.WHITE);
            }
        } else {
            LB_ESTADOFACTURA.setOpaque(false);
            LB_ESTADOFACTURA.setText(null);
        }
    }

    private void BUSCAR_FACTURA(int opcion) {
        int numfactura = edicion.toNumeroEntero(txtnumfactura.getText());
        if (opcion == 0) {
            numfactura = numfactura - 1;
        }
        if (opcion == 1) {
            numfactura = numfactura + 1;
        }
        CARGAR_FACTURA_VENTA(numfactura);
    }

    private void LLENAR_CLIENTE(Cliente cliente) {
        txtidentificacion.setText(cliente.getIdentificacion());
        txtnombreape.setText(cliente.getNombrecompleto());
        txttelefono.setText(cliente.getTelefono());
        txtdireccion.setText(cliente.getDireccion());
    }

}

class validador {

    public static java.util.HashMap<String, javax.swing.JInternalFrame> jIframes = new java.util.HashMap<>();

    public static void addJIframe(String key, javax.swing.JInternalFrame jiframe) {
        jIframes.put(key, jiframe);
    }

    public static javax.swing.JInternalFrame getJInternalFrame(String key) {
        return jIframes.get(key);
    }
}
