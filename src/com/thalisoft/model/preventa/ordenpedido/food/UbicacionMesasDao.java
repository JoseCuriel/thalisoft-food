package com.thalisoft.model.preventa.ordenpedido.food;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.database;
import com.thalisoft.model.maestros.empleado.EmpleadoDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ThaliSoft
 */
public class UbicacionMesasDao extends database {
    
    public boolean CRUD_UBICACION(Object[] values) {
        return EJECUTAR_SP("CRUD_UBICACIONMESAS", values);
    }
    
    public UbicacionMesas SELECT_UBICACION_MESAS(Object key) {
        UbicacionMesas ubicacionMesas = null;
        Object[][] rs = SELECT_SP("SELECT_UBICACION_MESAS", key);
        if (rs.length > 0) {
            ubicacionMesas = new UbicacionMesas();
            ubicacionMesas.setIdUbicacionMesas(Integer.parseInt(rs[0][0].toString()));
            ubicacionMesas.setDescripcionUbicacion(rs[0][1].toString());
            ubicacionMesas.setCantMesas(Integer.parseInt(rs[0][2].toString()));
            ubicacionMesas.setFechaHoraRegistro(DateUtil.getDateTime(rs[0][3]));
            ubicacionMesas.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][4]));
        }
        return ubicacionMesas;
    }
    
    public List<UbicacionMesas> LISTAR_UBICACION_MESAS(Object key) {
        UbicacionMesas ubicacionMesas;
        List<UbicacionMesas> list = new ArrayList<>();
        Object[][] rs = SELECT_SP("SELECT_UBICACION_MESAS", key);
        for (Object[] r : rs) {
            ubicacionMesas = new UbicacionMesas();
            ubicacionMesas.setIdUbicacionMesas(Integer.parseInt(r[0].toString()));
            ubicacionMesas.setDescripcionUbicacion(r[1].toString());
            ubicacionMesas.setCantMesas(Integer.parseInt(r[2].toString()));
            ubicacionMesas.setFechaHoraRegistro(DateUtil.getDateTime(r[3]));
            ubicacionMesas.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(r[4]));
            list.add(ubicacionMesas);
        }
        return list;
    }
    
    public String ID_UBICACION_MESA() {
        return SELECT_SP("SELECT_UBICACION_MESAS", "0,0")[0][0].toString();
    }
}
