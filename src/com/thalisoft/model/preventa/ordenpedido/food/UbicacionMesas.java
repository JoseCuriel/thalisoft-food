package com.thalisoft.model.preventa.ordenpedido.food;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;

/**
 *
 * @author ThaliSoft
 */
public class UbicacionMesas {
    
    private int idUbicacionMesas;
    private String descripcionUbicacion;
    private int cantMesas;
    private Date fechaHoraRegistro;
    private Empleado empleadoRegistra;

    public int getIdUbicacionMesas() {
        return idUbicacionMesas;
    }

    public void setIdUbicacionMesas(int idUbicacionMesas) {
        this.idUbicacionMesas = idUbicacionMesas;
    }

    public String getDescripcionUbicacion() {
        return descripcionUbicacion;
    }

    public void setDescripcionUbicacion(String descripcionUbicacion) {
        this.descripcionUbicacion = descripcionUbicacion;
    }

    public int getCantMesas() {
        return cantMesas;
    }

    public void setCantMesas(int cantMesas) {
        this.cantMesas = cantMesas;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }
       
}
