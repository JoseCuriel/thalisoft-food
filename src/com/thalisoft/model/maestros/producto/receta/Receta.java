package com.thalisoft.model.maestros.producto.receta;

import com.thalisoft.model.maestros.empleado.Empleado;
import com.thalisoft.model.maestros.producto.Producto;
import java.util.Date;

public class Receta {

    private int idReceta;
    private String tituloReceta;
    private Date FechaHoraRegistro;
    private Producto productoTerminado;
    private Date tiempoPreparacion;
    private Producto ingrediente;
    private int cantidad;
    private int costoProporcional;
    private Date FechaHoraRegIngrediente;
    private Empleado empleadoRegistra;

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public String getTituloReceta() {
        return tituloReceta;
    }

    public void setTituloReceta(String tituloReceta) {
        this.tituloReceta = tituloReceta;
    }

    public Date getFechaHoraRegistro() {
        return FechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date FechaHoraRegistro) {
        this.FechaHoraRegistro = FechaHoraRegistro;
    }

    public Producto getProductoTerminado() {
        return productoTerminado;
    }

    public void setProductoTerminado(Producto productoTerminado) {
        this.productoTerminado = productoTerminado;
    }

    public Date getTiempoPreparacion() {
        return tiempoPreparacion;
    }

    public void setTiempoPreparacion(Date tiempoPreparacion) {
        this.tiempoPreparacion = tiempoPreparacion;
    }

    public Producto getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(Producto ingrediente) {
        this.ingrediente = ingrediente;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCostoProporcional() {
        return costoProporcional;
    }

    public void setCostoProporcional(int costoProporcional) {
        this.costoProporcional = costoProporcional;
    }

    public Date getFechaHoraRegIngrediente() {
        return FechaHoraRegIngrediente;
    }

    public void setFechaHoraRegIngrediente(Date FechaHoraRegIngrediente) {
        this.FechaHoraRegIngrediente = FechaHoraRegIngrediente;
    }

    public Empleado getEmpleadoRegistra() {
        return empleadoRegistra;
    }

    public void setEmpleadoRegistra(Empleado empleadoRegistra) {
        this.empleadoRegistra = empleadoRegistra;
    }

}
