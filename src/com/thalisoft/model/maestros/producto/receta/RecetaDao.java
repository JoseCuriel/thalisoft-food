package com.thalisoft.model.maestros.producto.receta;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.database;
import com.thalisoft.model.maestros.empleado.EmpleadoDao;
import com.thalisoft.model.maestros.producto.Producto;
import com.thalisoft.model.maestros.producto.ProductoDao;

public class RecetaDao extends database {

    public boolean CRUD_RECETA(Object[] values) {
        return EJECUTAR_SP("CRUD_RECETA", values);
    }

    public Receta SELECT_RECETA(Object key) {
        Receta receta = null;
        Object[][] rs = SELECT_SP("SELECT_RECETA", key);
        if (rs.length > 0) {
            receta = new Receta();
            receta.setIdReceta(Integer.parseInt(rs[0][0].toString()));
            receta.setTituloReceta(rs[0][1].toString());
            receta.setProductoTerminado(new ProductoDao().READ_PRODUCTO(rs[0][2]));
            receta.setEmpleadoRegistra(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][3]));
            receta.setFechaHoraRegistro(DateUtil.getDateTime(rs[0][4]));
        }
        return receta;
    }

    public Object[][] LISTADO_INGREDIENTES(Object key) {
        return SELECT_SP("SELECT_RECETA", key);
    }

    public boolean ELIMINAR_INGREDIENTE(Object key) {
        return Delete("ingredientes_recetas", "ingredientes_recetas.idingrdiente = " + key);
    }

    public String NUMERO_RECETA() {
        return SELECT_SP("SELECT_RECETA", "2,0")[0][0].toString();
    }

    public float TOTAL_COSTO_PORCION(float cnt, float costoPorcion) {
        return Float.parseFloat(select(null, " ROUND("+cnt + "*" + costoPorcion+",2)", null)[0][0].toString());
    }
    
    public boolean ACTUALIZAR_COSTO_PRODUCTO_FINAL(Object[] values){
        return EJECUTAR_SP("SELECT_RECETA", values);
    }
    
     public Object[][] LISTADO_RECETAS(Object key) {
        return SELECT_SP("SELECT_RECETA", key);
    }

}
