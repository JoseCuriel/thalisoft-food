package com.thalisoft.model.maestros.producto;

import com.thalisoft.main.util.database;
import java.util.ArrayList;
import java.util.List;

public class UnidadMedidaDao extends database {

    public boolean CRUD_UNIDAD_MEDIDA(Object[] value) {
        return EJECUTAR_SP("CRUD_UNIDAD_MEDIDA", value);
    }

    public UnidadMedida SELECT_UNIDAD_MEDIDA(Object key) {
        UnidadMedida um = null;
        Object[][] rs = SELECT_SP("SELECT_UNIDAD_MEDIDA", key);
        if (rs.length > 0) {
            um = new UnidadMedida();
            um.setIdUnidadMedida(Integer.parseInt(rs[0][0].toString()));
            um.setMagnitud(rs[0][1].toString());
            um.setEquivalencia(Float.parseFloat(rs[0][2].toString()));
            um.setSimbolo(rs[0][3].toString());
        }
        return um;
    }

    public List<UnidadMedida> LISTADO_UNIDAD_MEDIDA() {
        UnidadMedida um;
        List<UnidadMedida> list = new ArrayList<>();
        Object[][] rs = SELECT_SP("SELECT_UNIDAD_MEDIDA", "0,0");
        for (Object[] r : rs) {
            um = new UnidadMedida();
            um.setIdUnidadMedida(Integer.parseInt(r[0].toString()));
            um.setMagnitud(r[1].toString());
            um.setEquivalencia(Float.parseFloat(r[2].toString()));
            um.setSimbolo(r[3].toString());
            list.add(um);
        }
        return list;
    }
}
