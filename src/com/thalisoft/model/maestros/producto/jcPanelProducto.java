package com.thalisoft.model.maestros.producto;

import com.lowagie.text.Rectangle;
import com.thalisoft.vista.maestros.producto.jpComponente;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.plaf.PanelUI;

/**
 * @web http://www.jc-mouse.net
 * @author Mouse
 */
public class jcPanelProducto extends JPanel implements ActionListener {

    private int index = 1;
    //Nos sirve para almacenar a los objetos creados
    private Map nota;
    CategoriaDao categoriaDao = new CategoriaDao();

    public jcPanelProducto() {
        this.setSize(600, 400);
        this.setVisible(true);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.setLayout(new GridLayout(0, 2));

        nota = new HashMap();
    }

    public void Mi_Componente(String tituloBtn) {
        //instancia nueva a componente

        jpComponente jpc = new jpComponente();
        jpc.btn.addActionListener(this);//escucha eventos
        jpc.btn.setText(tituloBtn);
        jpc.btn.setToolTipText(tituloBtn);
        this.add(jpc);//se añade al jpanel
        this.validate();
        //se añade al MAP

        this.nota.put(tituloBtn, jpc);
        //se incrementa contador de componentes
        index++;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //se obtiene el comando ejecutado
        try {

            String comando = e.getActionCommand();
            System.out.println("Comando: " + comando);
            //se recorre el MAP

            Iterator it = nota.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                //se obtiene el KEY -> identificador unico
                String itm = entry.getKey().toString();
                //si comando de componente es igual a comando pulsado
                if (itm.equals(comando)) {
                    //se recupera el contenido del JTextfield
                    for (SubCategoria LISTADO_DE_SUBCATEGORIAS : categoriaDao.LISTADO_DE_SUBCATEGORIAS("2,'" + itm + "'")) {
                        //322 725 9804System.out.println(LISTADO_DE_SUBCATEGORIAS.getDescripcion());
                        Mi_SubComponente(LISTADO_DE_SUBCATEGORIAS.getDescripcion());

                    }
                    //mostramos resultado
                    //JOptionPane.showMessageDialog(null, "Se presiono boton " + itm + " \n Hola ");
                }
            }

        } catch (Exception error) {
            System.out.println("error en la accion: ".toUpperCase() + error);
        }
    }

    public void Mi_SubComponente(String tituloBtn) {

        jpComponente c = new jpComponente();
        removeAll();
        
        c.btn.addActionListener(this);
        c.btn.setText(tituloBtn);
        c.btn.setToolTipText(tituloBtn);
        this.add(c);//se añade al jpanel
        this.validate();
        //se añade al MAP
        this.nota.put(tituloBtn, c);
        //se incrementa contador de componentes

    }

}
