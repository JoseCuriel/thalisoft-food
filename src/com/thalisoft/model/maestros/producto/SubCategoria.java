package com.thalisoft.model.maestros.producto;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;

public class SubCategoria {
    
    private int idSubCategoria;
    private String Descripcion;
    private Date fechaHoraRegistro;
    private Empleado empleadoSubCat;
    private Categoria categoria;

    public SubCategoria(int idSubCategoria, String Descripcion, Date fechaHoraRegistro, Empleado empleadoSubCat, Categoria categoria) {
        this.idSubCategoria = idSubCategoria;
        this.Descripcion = Descripcion;
        this.fechaHoraRegistro = fechaHoraRegistro;
        this.empleadoSubCat = empleadoSubCat;
        this.categoria = categoria;
    }

    public SubCategoria() {
    }

    public int getIdSubCategoria() {
        return idSubCategoria;
    }

    public void setIdSubCategoria(int idSubCategoria) {
        this.idSubCategoria = idSubCategoria;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Empleado getEmpleadoSubCat() {
        return empleadoSubCat;
    }

    public void setEmpleadoSubCat(Empleado empleadoSubCat) {
        this.empleadoSubCat = empleadoSubCat;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
 }
