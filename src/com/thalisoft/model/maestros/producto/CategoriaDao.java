package com.thalisoft.model.maestros.producto;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.Edicion;
import com.thalisoft.main.util.database;
import com.thalisoft.model.maestros.empleado.EmpleadoDao;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDao extends database {

    public boolean CRUD_CATEGORIA(Object[] value) {
        return EJECUTAR_SP("CRUD_CATEGORIA", value);
    }

    public boolean CRUD_SUBCATEGORIA(Object[] value) {
        return EJECUTAR_SP("CRUD_SUBCATEGORIA", value);
    }

    public Categoria SELECT_CATEGORIA(Object key) {
        Categoria categoria = null;
        Object[][] rs = SELECT_SP("SELECT_CATEGORIA", key);
        if (rs.length > 0) {
            categoria = new Categoria();
            categoria.setIdCategoria(Integer.parseInt(rs[0][0].toString()));
            categoria.setDescripcion(rs[0][1].toString());
            categoria.setFechaHoraRegistro(DateUtil.getDateTime(rs[0][2]));
            categoria.setEmpleadoRegCategoria(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][3]));
        }
        return categoria;
    }

    public List<Categoria> LISTADO_DE_CATEGORIAS() {
        Categoria categoria;
        List<Categoria> list = new ArrayList<>();
        Object[][] rs = SELECT_SP("SELECT_CATEGORIA", "1,1");
        for (Object[] r : rs) {
            categoria = new Categoria(Integer.parseInt(r[0].toString()), r[1].toString(), DateUtil.getDateTime(r[2]), new EmpleadoDao().CONSULTAR_EMPLEADO(r[3]));
            list.add(categoria);
        }
        return list;
    }

    public List<SubCategoria> LISTADO_DE_SUBCATEGORIAS(Object key) {
        SubCategoria subCategoria;
        List<SubCategoria> list = new ArrayList<>();
        Object[][] rs = SELECT_SP("SELECT_CATEGORIA", key);
        for (Object[] r : rs) {
            subCategoria = new SubCategoria(Integer.parseInt(r[0].toString()), r[1].toString(), DateUtil.getDateTime(r[3]), new EmpleadoDao().CONSULTAR_EMPLEADO(r[2]),
                    SELECT_CATEGORIA("0,'" + r[4] + "'"));
            list.add(subCategoria);
        }
        return list;
    }

    public SubCategoria SELECT_SUBCATEGORIA(Object key) {
        SubCategoria subCategoria = null;
        Object[][] rs = SELECT_SP("SELECT_CATEGORIA", key);
        if (rs.length > 0) {
            subCategoria = new SubCategoria(Integer.parseInt(rs[0][0].toString()), rs[0][1].toString(),
                    DateUtil.getDateTime(rs[0][2]), new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][3]), SELECT_CATEGORIA("0," + rs[0][4]));
        }
        return subCategoria;
    }

    public Object[][] CARGAR_LISTA_CATEGORIAS(Object key) {
        return SELECT_SP("SELECT_CATEGORIA", key);
    }

    public List<Producto> LISTA_DE_PRODUCTOS_POR_CATEGORIA(String categoria) {
        Producto producto;
        List<Producto> list = new ArrayList<>();
        Object[][] rs = SELECT_SP("SELECT_CATEGORIA", categoria);
        Edicion edicion = new Edicion();
        for (Object[] r : rs) {

            EmpleadoDao empleado = new EmpleadoDao();
            producto = new Producto();
            CategoriaDao categoriaDao = new CategoriaDao();
            producto.setId_producto(edicion.toNumeroEntero(r[0].toString()));
            producto.setReferencia(r[1].toString());
            producto.setDescripcion(r[2].toString());
            producto.setStrock(edicion.toNumeroEntero(r[3].toString()));
            producto.setCosto_und(edicion.toNumeroEntero(r[4].toString()));
            producto.setPrecio_venta(edicion.toNumeroEntero(r[5].toString()));
            producto.setUtilidad(edicion.toNumeroEntero(r[6].toString()));
            producto.setTipoVenta(r[7].toString());
            producto.setUndMedida(r[8].toString());
            producto.setStockMinimo(edicion.toNumeroEntero(r[9].toString()));
            producto.setFechahoraingreso(DateUtil.getDateTime(r[10]));
            producto.setEmpleado(empleado.CONSULTAR_EMPLEADO(r[11]));
            producto.setSubCategoria(categoriaDao.SELECT_SUBCATEGORIA("3,'" + r[12] + "'"));
            producto.setImagen(r[13].toString());
            producto.setPum(Float.parseFloat(r[14].toString()));
            producto.setEmbalaje(Float.parseFloat(r[15].toString()));
            list.add(producto);
        }

        return list;
    }

}
