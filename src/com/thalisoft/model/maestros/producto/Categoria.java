package com.thalisoft.model.maestros.producto;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;


public class Categoria {
    
    private int idCategoria;
    private String Descripcion;
    private Date fechaHoraRegistro;
    private Empleado empleadoRegCategoria;

    public Categoria(int idCategoria, String Descripcion, Date fechaHoraRegistro, Empleado empleadoRegCategoria) {
        this.idCategoria = idCategoria;
        this.Descripcion = Descripcion;
        this.fechaHoraRegistro = fechaHoraRegistro;
        this.empleadoRegCategoria = empleadoRegCategoria;
    }

    Categoria() {

    }
    
    

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Empleado getEmpleadoRegCategoria() {
        return empleadoRegCategoria;
    }

    public void setEmpleadoRegCategoria(Empleado empleadoRegCategoria) {
        this.empleadoRegCategoria = empleadoRegCategoria;
    }

    
    
}
