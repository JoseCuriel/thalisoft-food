package com.thalisoft.model.maestros.producto;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;

public class Producto {

    private int id_producto;
    private String referencia;
    private String descripcion;
    private int strock;
    private float costo_und;
    private int precio_venta;
    private int utilidad;
    private String tipoVenta;
    private String undMedida;
    private int stockMinimo;
    private Date fechahoraingreso;
    private Empleado empleado;
    private SubCategoria subCategoria;
    private String imagen;
    private float pum;
    private float embalaje;

    public Producto() {
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStrock() {
        return strock;
    }

    public void setStrock(int strock) {
        this.strock = strock;
    }

    public float getCosto_und() {
        return costo_und;
    }

    public void setCosto_und(float costo_und) {
        this.costo_und = costo_und;
    }

    public int getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(int precio_venta) {
        this.precio_venta = precio_venta;
    }

    public int getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(int utilidad) {
        this.utilidad = utilidad;
    }

    public String getTipoVenta() {
        return tipoVenta;
    }

    public void setTipoVenta(String tipoVenta) {
        this.tipoVenta = tipoVenta;
    }

    public String getUndMedida() {
        return undMedida;
    }

    public void setUndMedida(String undMedida) {
        this.undMedida = undMedida;
    }

    public int getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(int stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Date getFechahoraingreso() {
        return fechahoraingreso;
    }

    public void setFechahoraingreso(Date fechahoraingreso) {
        this.fechahoraingreso = fechahoraingreso;
    }

    public SubCategoria getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(SubCategoria subCategoria) {
        this.subCategoria = subCategoria;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public float getPum() {
        return pum;
    }

    public void setPum(float pum) {
        this.pum = pum;
    }

    public float getEmbalaje() {
        return embalaje;
    }

    public void setEmbalaje(float embalaje) {
        this.embalaje = embalaje;
    }


}
