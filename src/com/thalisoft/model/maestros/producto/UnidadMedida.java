package com.thalisoft.model.maestros.producto;

public class UnidadMedida {

    private int idUnidadMedida;
    private String magnitud;
    private float equivalencia;
    private String simbolo;

    public int getIdUnidadMedida() {
        return idUnidadMedida;
    }

    public void setIdUnidadMedida(int idUnidadMedida) {
        this.idUnidadMedida = idUnidadMedida;
    }

    public String getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(String magnitud) {
        this.magnitud = magnitud;
    }

    public float getEquivalencia() {
        return equivalencia;
    }

    public void setEquivalencia(float equivalencia) {
        this.equivalencia = equivalencia;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }
    
    
    
}
