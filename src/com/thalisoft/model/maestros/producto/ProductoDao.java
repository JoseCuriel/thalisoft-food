package com.thalisoft.model.maestros.producto;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.Edicion;
import com.thalisoft.main.util.database;
import com.thalisoft.model.maestros.empleado.EmpleadoDao;
import java.util.ArrayList;
import java.util.List;

public class ProductoDao extends database {

    Edicion edicion = new Edicion();
    EmpleadoDao empleado;

    public boolean EJECUTAR_CRUD(Object[] key) {
        return EJECUTAR_SP("CRUD_PRODUCTO", key);
    }

    public Producto READ_PRODUCTO(Object key) {
        Object parametro = 1 + ",'" + key + "'";
        Producto producto = null;
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO", parametro);
        if (rs.length > 0) {
            empleado = new EmpleadoDao();
            producto = new Producto();
            CategoriaDao categoriaDao = new CategoriaDao();
            producto.setId_producto(edicion.toNumeroEntero(rs[0][0].toString()));
            producto.setReferencia(rs[0][1].toString());
            producto.setDescripcion(rs[0][2].toString());
            producto.setStrock(edicion.toNumeroEntero(rs[0][3].toString()));
            producto.setCosto_und(edicion.toNumeroFloat(rs[0][4].toString()));
            producto.setPrecio_venta(edicion.toNumeroEntero(rs[0][5].toString()));
            producto.setUtilidad(edicion.toNumeroEntero(rs[0][6].toString()));
            producto.setTipoVenta(rs[0][7].toString());
            producto.setUndMedida(rs[0][8].toString());
            producto.setStockMinimo(edicion.toNumeroEntero(rs[0][9].toString()));
            producto.setFechahoraingreso(DateUtil.getDateTime(rs[0][10]));
            producto.setEmpleado(empleado.CONSULTAR_EMPLEADO(rs[0][11]));
            producto.setSubCategoria(categoriaDao.SELECT_SUBCATEGORIA("3," + rs[0][12]));
            producto.setImagen(rs[0][13].toString());
            producto.setPum(Float.parseFloat(rs[0][14].toString()));
            producto.setEmbalaje(Float.parseFloat(rs[0][15].toString()));

        }
        return producto;
    }

    public Object[][] LISTADO_DE_PRODUCTOS() {
        Object parametro = 2 + "," + 0;
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO", parametro);
        if (rs.length > 0) {
            return rs;
        }
        return null;
    }

    public List<Producto> LISTA_PRODUCTOS() {
        List<Producto> list = new  ArrayList<>();
        Producto producto;
        Object[][] rs = LISTADO_DE_PRODUCTOS();
        if (rs.length > 0) {
            list = new ArrayList<>();
            for (Object[] r : rs) {
                empleado = new EmpleadoDao();
                EmpleadoDao empleadoDao = new EmpleadoDao();
                producto = new Producto();
                CategoriaDao categoriaDao = new CategoriaDao();
                producto.setId_producto(edicion.toNumeroEntero(r[0].toString()));
                producto.setReferencia(r[1].toString());
                producto.setDescripcion(r[2].toString());
                producto.setStrock(edicion.toNumeroEntero(r[3].toString()));
                producto.setCosto_und(edicion.toNumeroFloat(r[4].toString()));
                producto.setPrecio_venta(edicion.toNumeroEntero(r[5].toString()));
                producto.setUtilidad(edicion.toNumeroEntero(r[6].toString()));
                producto.setTipoVenta(r[7].toString());
                producto.setUndMedida(r[8].toString());
                producto.setStockMinimo(edicion.toNumeroEntero(r[9].toString()));
                producto.setFechahoraingreso(DateUtil.getDateTime(r[10]));
                producto.setEmpleado(empleadoDao.CONSULTAR_EMPLEADO(r[11]));
                producto.setSubCategoria(categoriaDao.SELECT_SUBCATEGORIA("3,'" + r[12] + "'"));
                producto.setImagen(r[13].toString());
                producto.setPum(Float.parseFloat(r[14].toString()));
                producto.setEmbalaje(Float.parseFloat(r[15].toString()));
                list.add(producto);

            }

        }
        return list;
    }

    public String NUMERO_FICHA_PRODUCTO() {
        Object parametro = 3 + "," + 0;
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO", parametro);
        if (rs.length > 0) {
            return rs[0][0].toString();
        }
        return null;
    }
}
