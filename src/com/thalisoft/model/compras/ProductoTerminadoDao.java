package com.thalisoft.model.compras;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.database;
import com.thalisoft.model.maestros.empleado.Empleado;
import com.thalisoft.model.maestros.empleado.EmpleadoDao;

public class ProductoTerminadoDao extends database {

    public boolean CRUD_ENTRADA_PRODUCTO(Object[] values) {
        return EJECUTAR_SP("CRUD_ENTRAR_PRODUCTO_FINAL", values);
    }

    public ProductoTerminado CONSULTA_PRODUCTO_TERMINADO(Object key) {
        ProductoTerminado pt = null;
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO_TERMINADO", key);
        if (rs.length > 0) {
            pt = new ProductoTerminado();
            pt.setIdentrada(Integer.parseInt(rs[0][0].toString()));
            pt.setFechaEntrada(DateUtil.getDateTime(rs[0][1]));
            pt.setEmpleado(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][2]));
            pt.setFechahoraregistro(DateUtil.getDateTime(rs[0][3]));
            pt.setEstadoEntrada(Integer.parseInt(rs[0][4].toString()));
        }
        return pt;
    }

    public Object[][] DETALLE_PRODUCTO_TERMINADO(Object key) {
        Object[][] rs = SELECT_SP("SELECT_PRODUCTO_TERMINADO", key);
        if (rs.length > 0) {
            return rs;
        }
        return null;
    }
    
    public String NUMERO_ENTRADA_PRODUCTO_FINAL() {
        return SELECT_SP("SELECT_PRODUCTO_TERMINADO", "2,0")[0][0].toString();        
    }

    public boolean BORRAR_PRODUCTO_DETALLE(int idcompra) {
         return Delete("detalleentradas", "iddetalleentrada = "+idcompra+"");
    }
}
