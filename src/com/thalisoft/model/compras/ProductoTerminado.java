package com.thalisoft.model.compras;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;


public class ProductoTerminado {
    
    private int identrada;
    private Date fechaEntrada;
    private Date fechahoraregistro;
    private Empleado empleado;
    private int estadoEntrada;

    public int getIdentrada() {
        return identrada;
    }

    public void setIdentrada(int identrada) {
        this.identrada = identrada;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Date getFechahoraregistro() {
        return fechahoraregistro;
    }

    public void setFechahoraregistro(Date fechahoraregistro) {
        this.fechahoraregistro = fechahoraregistro;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public int getEstadoEntrada() {
        return estadoEntrada;
    }

    public void setEstadoEntrada(int estadoEntrada) {
        this.estadoEntrada = estadoEntrada;
    }
    
    
}
