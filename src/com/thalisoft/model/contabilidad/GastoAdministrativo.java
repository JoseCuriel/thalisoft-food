package com.thalisoft.model.contabilidad;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;

public class GastoAdministrativo {
    private int idGastoAdmin;
    private Date fechahoraregistro;
    private String tipogasto;
    private String descripcion;
    private int valorTotal;
    private Empleado empleado;

    public int getIdGastoAdmin() {
        return idGastoAdmin;
    }

    public void setIdGastoAdmin(int idGastoAdmin) {
        this.idGastoAdmin = idGastoAdmin;
    }

    public Date getFechahoraregistro() {
        return fechahoraregistro;
    }

    public void setFechahoraregistro(Date fechahoraregistro) {
        this.fechahoraregistro = fechahoraregistro;
    }

    public String getTipogasto() {
        return tipogasto;
    }

    public void setTipogasto(String tipogasto) {
        this.tipogasto = tipogasto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(int valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
}
