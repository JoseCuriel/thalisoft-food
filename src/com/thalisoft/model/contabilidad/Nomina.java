package com.thalisoft.model.contabilidad;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;

public class Nomina {
    
    private int idLiquidacion;
    private Date fechaliquidacion;
    private String tipoNomina;
    private int descuento;
    private int diasLaborados;
    private int TotalNetoPago;
    private String Observaciones;
    private Date fechaHoraRegistro;
    private Empleado empleado;
    private Empleado usuarioRegistra;

    public int getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(int idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Date getFechaliquidacion() {
        return fechaliquidacion;
    }

    public void setFechaliquidacion(Date fechaliquidacion) {
        this.fechaliquidacion = fechaliquidacion;
    }

    public String getTipoNomina() {
        return tipoNomina;
    }

    public void setTipoNomina(String tipoNomina) {
        this.tipoNomina = tipoNomina;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public int getDiasLaborados() {
        return diasLaborados;
    }

    public void setDiasLaborados(int diasLaborados) {
        this.diasLaborados = diasLaborados;
    }

    public int getTotalNetoPago() {
        return TotalNetoPago;
    }

    public void setTotalNetoPago(int TotalNetoPago) {
        this.TotalNetoPago = TotalNetoPago;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String Observaciones) {
        this.Observaciones = Observaciones;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Empleado getUsuarioRegistra() {
        return usuarioRegistra;
    }

    public void setUsuarioRegistra(Empleado usuarioRegistra) {
        this.usuarioRegistra = usuarioRegistra;
    }

}
