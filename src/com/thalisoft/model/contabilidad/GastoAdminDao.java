package com.thalisoft.model.contabilidad;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.database;

public class GastoAdminDao extends database {
    
    public boolean CRUD_GASTO_ADMIN(Object[] key) {
        return EJECUTAR_SP("CRUD_GASTO_ADMIN", key);
    }
    
    public GastoAdministrativo CONSULTA_GASTO_ADMIN(Object key) {
        GastoAdministrativo ga = null;
        Object[][] rs = SELECT_SP("SELECT_GASTO_ADMIN", key);
        if (rs.length != 0) {
            ga = new GastoAdministrativo();
            ga.setIdGastoAdmin(Integer.parseInt(rs[0][0].toString()));
            ga.setFechahoraregistro(DateUtil.getDateTime(rs[0][1]));
            ga.setTipogasto(rs[0][2].toString());
            ga.setDescripcion(rs[0][3].toString());
            ga.setValorTotal(Integer.parseInt(rs[0][4].toString()));
        }
        return ga;
    }
    
    public String NUMERO_GASTO_ADMIN() {
        return SELECT_SP("SELECT_GASTO_ADMIN", "0,0,null,null")[0][0].toString();
    }

    public Object[][] LISTAR_GASTOS_REGISTRADOS(String string) {
        return SELECT_SP("SELECT_GASTO_ADMIN", string);
    }
}
