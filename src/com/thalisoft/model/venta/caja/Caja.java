package com.thalisoft.model.venta.caja;

import com.thalisoft.model.maestros.empleado.Empleado;
import java.util.Date;


public class Caja {
    
    private int idcaja;
    private Date fechahoraapertura;
    private Date fechahoracierre;
    private int saldoInicial;
    private int estadoCaja;
    private Empleado empleadoAbre;
    private Empleado empleadoCierra;

    public int getIdcaja() {
        return idcaja;
    }

    public void setIdcaja(int idcaja) {
        this.idcaja = idcaja;
    }

    public Date getFechahoraapertura() {
        return fechahoraapertura;
    }

    public void setFechahoraapertura(Date fechahoraapertura) {
        this.fechahoraapertura = fechahoraapertura;
    }

    public Date getFechahoracierre() {
        return fechahoracierre;
    }

    public void setFechahoracierre(Date fechahoracierre) {
        this.fechahoracierre = fechahoracierre;
    }

    public int getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(int saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public int getEstadoCaja() {
        return estadoCaja;
    }

    public void setEstadoCaja(int estadoCaja) {
        this.estadoCaja = estadoCaja;
    }

    public Empleado getEmpleadoAbre() {
        return empleadoAbre;
    }

    public void setEmpleadoAbre(Empleado empleadoAbre) {
        this.empleadoAbre = empleadoAbre;
    }

    public Empleado getEmpleadoCierra() {
        return empleadoCierra;
    }

    public void setEmpleadoCierra(Empleado empleadoCierra) {
        this.empleadoCierra = empleadoCierra;
    }
    
    
}
