package com.thalisoft.model.venta.caja;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.database;


public class CajaDao extends database {
    
    public boolean CRUD_CAJA(Object[] values) {
        return EJECUTAR_SP("CRUD_CAJA", values);
    }
    
    public Caja SELECT_CAJA(Object key) {
        Caja caja = null;
        Object[][] rs = SELECT_SP("SELECT_CAJA", key);
        if (rs.length > 0) {
            caja = new Caja();
            caja.setIdcaja(Integer.parseInt(rs[0][0].toString()));
            caja.setFechahoraapertura(DateUtil.getDateTime(rs[0][1]));
            caja.setFechahoracierre(DateUtil.getDateTime(rs[0][2]));
            caja.setSaldoInicial(Integer.parseInt(rs[0][3].toString()));
            caja.setEstadoCaja(Integer.parseInt(rs[0][4].toString()));
        }
        return caja;
    }
    
    public Object[][] DETALLE_CIERRE(Object key) {
        return SELECT_SP("SELECT_CAJA", key);
    }
    
    public String NUMERO_CAJA() {
        return SELECT_SP("SELECT_CAJA", "0,0")[0][0].toString();
    }
    
    public Object[][] DIVISAS_MONEDAS() {
        return SELECT_SP("SELECT_CAJA", "3,'MONEDAS'");
    }
    
    public Object[][] DIVISAS_BILLETES() {
        return SELECT_SP("SELECT_CAJA", "3,'BILLETES'");
    }
}
