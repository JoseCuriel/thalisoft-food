package com.thalisoft.model.venta;

import com.thalisoft.main.util.DateUtil;
import com.thalisoft.main.util.database;
import com.thalisoft.model.maestros.empleado.EmpleadoDao;

public class OfertaDao extends database {

    public boolean CRUD_OFERTA(Object[] values) {
        return EJECUTAR_SP("CRUD_OFERTAS", values);
    }

    public Oferta SELECT_OFERTA(Object key) {
        Oferta oferta = null;
        Object[][] rs = SELECT_SP("SELECT_OFERTAS", key);
        if (rs.length > 0) {
            oferta = new Oferta();
            oferta.setIdOferta(Integer.parseInt(rs[0][0].toString()));
            oferta.setFechainicial(DateUtil.getDateTime(rs[0][1]));
            oferta.setFechaFinal(DateUtil.getDateTime(rs[0][2]));
            oferta.setDescuento(Integer.parseInt(rs[0][3].toString()));
            oferta.setFechahoraregistro(DateUtil.getDateTime(rs[0][4]));
            oferta.setEmpleado(new EmpleadoDao().CONSULTAR_EMPLEADO(rs[0][5]));
        }
        return oferta;
    }

    public Object[][] SELECT_DETALLE_OFERTA(Object key) {
        return SELECT_SP("SELECT_OFERTAS", key);
    }

    public Object NUMERO_OFERTA() {
        return SELECT_SP("SELECT_OFERTAS", "0,0")[0][0];
    }
    
    public boolean ELIMINAR_DETALLE_OFERTA(int idDetalle) {
        return Delete("detalle_ofertas", "detalle_ofertas.itemdetalle = "+idDetalle+"");
    }
            
}
