SELECT
productos.referencia,
productos.descripcion,
productos.precioventa
FROM
productos
WHERE 
referencia NOT IN (SELECT detalle_ofertas.producto FROM detalle_ofertas, ofertas 
WHERE detalle_ofertas.oferta = ofertas.idoferta AND ofertas.idoferta = 1) 
ORDER BY productos.descripcion
