SELECT
detalle_ofertas.itemdetalle,
productos.referencia,
productos.descripcion,
productos.stockactual,
detalle_ofertas.precioventa,
detalle_ofertas.valordescuento,
productos.stockactual*
detalle_ofertas.precioventa AS TOTAL,
productos.categoria
FROM
ofertas
INNER JOIN detalle_ofertas ON detalle_ofertas.oferta = ofertas.idoferta
INNER JOIN productos ON detalle_ofertas.producto = productos.referencia
WHERE ofertas.idoferta = 1
